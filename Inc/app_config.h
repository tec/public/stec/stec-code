/*
 * Copyright (c) 2020 - 2021, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * STeC config
 */

#ifndef __APP_CONFIG_H
#define __APP_CONFIG_H


/* --- adjustable parameters --- */

/* general */
#define FW_NAME                         "DPP2STeC"   /* max. 8 chars */
#define FW_VERSION_MAJOR                1           /* 0..6 */
#define FW_VERSION_MINOR                0           /* 0..99 */
#define FW_VERSION_PATCH                1           /* 0..99 */
#define FW_VERSION                      (FW_VERSION_MAJOR * 10000 + FW_VERSION_MINOR * 100 + FW_VERSION_PATCH)    /* 16-bit value */

#define FLOCKLAB                        1           /* set to 1 to run on FlockLab */
#define BASEBOARD                       0           /* set to 1 if the comboard will be installed on a baseboard */
#define BASESTATION                     0           /* set to 1 for a base station */
#if BASESTATION
  #define BASESTATION_FALLBACK          0           /* set to 1 to use the fallback radio config for the base station */
  #define BASESTATION_SNIFFER           0           /* set to 1 to use the base station in sniffer mode (receive only) */
#endif /* BASESTATION */

/* node ID and network parameters */
#define NUM_HOPS                        2           /* estimated network diameter in number of hops (only used to calculate the required Gloria slot duration) */
#if BASESTATION
  #define NODE_ID                       BASESTATION_ID
  #define BS_MIN_NODE_ID                21000       /* for base station: smallest expected node ID in the network, must be <= MIN_NODE_ID */
  #define BS_MAX_NODE_ID                21099       /* for base station: largest expected node ID in the network, must be >= MIN_NODE_ID + MAX_NUM_NODES */
#elif !FLOCKLAB
  #define NODE_ID                       21018       /* source node ID */
#endif /* FLOCKLAB */
#define MAX_NUM_NODES                   16          /* max. number of nodes in the network, only relevant for source nodes */
#if FLOCKLAB
  #define BASESTATION_ID                2
  #define MIN_NODE_ID                   2           /* smallest node ID in the network (on FlockLab, node IDs range from 1 to ~30) */
#else /* FLOCKLAB */
  #define BASESTATION_ID                108         /* only relevant if the code is compiled for the base station */
  #define MIN_NODE_ID                   21006       /* smallest node ID in the network, only relevant for source nodes */
#endif /* FLOCKLAB */
#define WRITE_NODE_ID                   0           /* 1 = force node ID overwrite, 0 = use ID stored in NV config if available */

/* energy (low-power mode) */
#if SWO_ENABLE || BASESTATION
  #define LOW_POWER_MODE                LP_MODE_SLEEP  /* low-power mode to use between rounds during periods of inactivity */
#else /* SWO_ENABLE */
  #define LOW_POWER_MODE                LP_MODE_STOP2  /* low-power mode to use between rounds during periods of inactivity */
#endif /* SWO_ENABLE */
#define LPM_DISABLE_GPIO_CLOCKS         0           /* set to 1 to disable GPIO clocks in low-power mode (-> no GPIO tracing possible) */
#define LPM_MIN_IDLE_TIME_MS            20          /* minimum expected idle time in order to enter LPM */
#define LPM_RADIO_COLD_SLEEP            1           /* set to 1 to enable cold sleep mode for the radio in STOP2 LPM or lower (saves ~500nA) */

/* time sync and drift compensation */
#define TIMESTAMP_TYPICAL_DRIFT         40    /* typical drift +/- in ppm (if exceeded, a warning will be issued) */
#define TIMESTAMP_MAX_DRIFT             100   /* max. allowed drift in ppm (higher values will be capped) */
#define TIMESTAMP_REQUEST_TIMEOUT       60    /* max. delay between time request and sending of the timestamp, in seconds (if elapsed time is larger, no timestamp will be sent to the APP). set to 0 to disable this feature */
#define TIMESTAMP_OFS_WARN_THRESHOLD_MS 100   /* issue a warning if the time was off by more than this threshold and has been adjusted */

/* data collection and logging */
#define EVENT_MSG_LEVEL                 EVENT_MSG_LEVEL_INFO            /* do not generate event messages */
#define EVENT_MSG_TARGET                EVENT_MSG_TARGET_BOLT           /* send all event messages via BOLT to APP for logging to the SD card */
#define SEND_NODE_INFO_MSG              1                               /* if set to 1, a source node will send a node info message after a reset */
#define NODE_HEALTH_MSG_INTERVAL        60                              /* interval in minutes at which node health messages will be generated / sent */
#define NODE_HEALTH_REPORTING_OFS_S     12                              /* offset between two health message reporting slots (takes ~10.3s for SF11 for 3 transmission attempts, including the wait-for-ACK timeouts) */
#define NODE_HEALTH_MIN_INTERVAL_S      (MAX_NUM_NODES * NODE_HEALTH_REPORTING_OFS_S)   /* min. allowed interval for the node health messages */
#define NODE_HEALTH_MAX_INTERVAL_S      (4 * 3600)                      /* max. allowed interval for the node health messages */
#define PS_TIMESTAMP()                  get_time(0)
#define PS_DEVICE_ID                    config.node_id

/* memory */
#define BOLT_TASK_STACK_SIZE            300                             /* in # words of 4 bytes */
#define TIMESYNC_TASK_STACK_SIZE        300                             /* in # words of 4 bytes */
#define COM_TASK_STACK_SIZE             500                             /* in # words of 4 bytes */
#define DEBUG_TASK_STACK_SIZE           320                             /* in # words of 4 bytes */
#define BASESTATION_TASK_STACK_SIZE     300                             /* in # words of 4 bytes */
#define STACK_WARNING_THRESHOLD         80                              /* a warning will be generated once the stack usage of a task exceeds this value (in percent) */
#define FSK_RX_QUEUE_SIZE               MAX_NUM_NODES                   /* #messages */
#define FSK_TX_QUEUE_SIZE               1                               /* #messages */
#if BASESTATION
  #define LORA_TX_QUEUE_SIZE            10                              /* #messages */
#else
  #define LORA_TX_QUEUE_SIZE            1                               /* #messages */
#endif /* BASESTATION */
#define LORA_RX_QUEUE_SIZE              1                               /* #messages */
#define EVENT_QUEUE_SIZE                10

/* FW update */
#if BASESTATION
  #define FW_OTA_ENABLE                 !BASESTATION_SNIFFER            /* don't enable FW update feature on sniffer nodes */
#else  /* BASESTATION */
  #define FW_OTA_ENABLE                 1                               /* set to 1 to enable FW updates over-the-air */
#endif /* BASESTATION */
#define FW_OTA_NODE_ID                  config.node_id
#define FW_OTA_TIMEOUT                  300           /* timeout in seconds after which the firmware update mode is exited (if no further data received) and normal operation is resumed */
#define FW_OTA_ALLOW_DOWNGRADE          0
#define FW_OTA_KEY                      10101         /* optional 16-bit key to activate FW update mode via command */
#define FW_OTA_MASTER                   BASESTATION   /* whether this node should act as a FW update master and store the FW for other nodes in its own flash memory */

/* non-volatile config storage */
#define NVCFG_ENABLE                    (!FLOCKLAB)   /* use non-volatile config storage (except on FlockLab) */
#define NVCFG_BLOCK_SIZE                16            /* note: must be sizeof(nv_config_t)! */
#define NVCFG_ONLY_SAVE_IF_CHANGED      1             /* only save the config if it has changed */

/* Gloria config */
#define GLORIA_INTERFACE_POWER          14   /* transmit power in dBm */
#define GLORIA_INTERFACE_MODULATION     10   /* FSK 250kbit/s (see radio_constants.c for details) */
#define GLORIA_INTERFACE_N_TX           2    /* number of retransmissions */
#if FLOCKLAB
  #define GLORIA_INTERFACE_RF_BAND      46   /* ~869.0 MHz (see table in radio_constants.c for options) */
#else
  #define GLORIA_INTERFACE_RF_BAND      43   /* ~868.4 MHz (see table in radio_constants.c for options) */
#endif /* FLOCKLAB */

/* Baseboard */
#if BASEBOARD
  #define BASEBOARD_TREQ_WATCHDOG       3600  /* if != 0, the baseboard will be power-cycled if no time request has been received within the specified #seconds */
  #define BOLT_WAKE_BASEBOARD_THRESHOLD 100   /* wake up the baseboard if x messages have been written to BOLT while the baseboard is disabled; set to 0 to disable this feature */
  #define BASEBOARD_CMD_QUEUE_SIZE      10    /* queue size for baseboard enable/disable commands */
  #define BASEBOARD_WAKEUP_PERIOD       86400 /* default wake-up period for the periodic service window, in seconds */
  #define BASEBOARD_WAKEUP_TIME         10    /* default offset (daytime hour UTC) for the periodic wakeup; set to 0 to disable the periodic wakeup */
#else /* BASEBOARD_TREQ_WATCHDOG */
  #define BASEBOARD_TREQ_WATCHDOG       0
#endif /* BASEBOARD_TREQ_WATCHDOG */

/* BOLT */
#define BOLT_ENABLE                     (!FLOCKLAB) /* BOLT is not available on FlockLab */
#if BASESTATION
  #define BOLT_MAX_READ_COUNT           100         /* max. number of messages to read from BOLT at once */
#else /* BASESTATION */
  #define BOLT_MAX_READ_COUNT           20          /* max. number of messages to read from BOLT at once */
#endif /* BASESTATION */
#define BOLT_TASK_YIELD_TIME_MS         20          /* how long a task should yield to let the BOLT task run */

/* misc (Flora Lib config) */
#define SWO_ENABLE                      0           /* set to 1 to enable data tracing or serial printing via SWO pin */
#define FLOCKLAB_SWD                    0           /* set to 1 to reserve SWDIO / SWDCLK pins for debugging (GPIOs not available for tracing) */
#define FLOCKLAB_SIG_INT                FLOCKLAB    /* enable SIG pin interrupt on FlockLab */
#define HS_TIMER_COMPENSATE_DRIFT       0
#define HS_TIMER_INIT_FROM_RTC          0
#define LPTIMER_RESET_WDG_ON_OVF        1
#define LPTIMER_RESET_WDG_ON_EXP        0
#define LPTIMER_CHECK_EXP_TIME          1
#define UART_FIFO_BUFFER_SIZE           1           /* not used, set to 1 byte to reduce memory usage */
#define CLI_ENABLE                      0           /* command line interface */

/* serial logging */
#define LOG_ENABLE                      1
#define LOG_LEVEL                       LOG_LEVEL_VERBOSE
#define LOG_BUFFER_SIZE                 8192
#if BASEBOARD
  #define LOG_ADD_TIMESTAMP             0           /* don't print the timestamp on the baseboard */
  #define LOG_USE_COLORS                0
  #define LOG_LEVEL_ERROR_STR           "<3>"       /* use syslog severity level number instead of strings */
  #define LOG_LEVEL_WARNING_STR         "<4>"
  #define LOG_LEVEL_INFO_STR            "<6>"
  #define LOG_LEVEL_VERBOSE_STR         "<7>"
#endif /* BASEBOARD */
#if SWO_ENABLE || FLOCKLAB
  #if SWO_ENABLE
    #define LOG_PRINT_FUNC              swo_print
  #endif /* SWO_ENABLE */
  #if FLOCKLAB
    #define LOG_ADD_TIMESTAMP           0           /* don't print the timestamp on FlockLab */
  #endif /* FLOCKLAB */
  #define LOG_PRINT_IMMEDIATELY         1
#else /* SWO_ENABLE || FLOCKLAB */
  #define LOG_PRINT_IMMEDIATELY         0           /* if set to zero, the debug task is responsible for printing out the debug messages via UART */
#endif /* SWO_ENABLE || FLOCKLAB */

/* GPIO indicators for tracing */
#if !BASEBOARD
  #if FLOCKLAB
    #define ISR_ON_IND()                bool nested = PIN_STATE(FLOCKLAB_INT1); (void)nested; PIN_SET(FLOCKLAB_INT1)    /* if unused, insert 2x NOP here */
    #define ISR_OFF_IND()               if (!nested) PIN_CLR(FLOCKLAB_INT1)
    #define GLORIA_START_IND()          led_on(LED_SYSTEM)
    #define GLORIA_STOP_IND()           led_off(LED_SYSTEM)
    #define RADIO_TX_START_IND()        PIN_SET(FLOCKLAB_LED2)
    #define RADIO_TX_STOP_IND()         PIN_CLR(FLOCKLAB_LED2)
    #define RADIO_RX_START_IND()        PIN_SET(FLOCKLAB_LED3)
    #define RADIO_RX_STOP_IND()         PIN_CLR(FLOCKLAB_LED3)
    #define COM_TASK_PHASE_START_IND()  PIN_SET(FLOCKLAB_INT2)
    #define COM_TASK_PHASE_STOP_IND()   PIN_CLR(FLOCKLAB_INT2)
  #else /* FLOCKLAB */
    #define CPU_ON_IND()                //PIN_SET(COM_GPIO1)
    #define CPU_OFF_IND()               //PIN_CLR(COM_GPIO1)
    #define LPM_ON_IND()                //PIN_CLR(COM_GPIO1)
    #define LPM_OFF_IND()               //PIN_SET(COM_GPIO1)
    #define IDLE_TASK_RESUMED()         //PIN_SET(COM_GPIO1)
    #define IDLE_TASK_SUSPENDED()       //PIN_CLR(COM_GPIO1)
    #define ISR_ON_IND()                PIN_SET(COM_GPIO1)      /* if unused, insert 2x NOP here */
    #define ISR_OFF_IND()               PIN_CLR(COM_GPIO1)
    #define BOLT_TASK_RESUMED()         //PIN_SET(COM_GPIO1)
    #define BOLT_TASK_SUSPENDED()       //PIN_CLR(COM_GPIO1)
    #define TIMESYNC_TASK_RESUMED()     //PIN_SET(COM_GPIO1)
    #define TIMESYNC_TASK_SUSPENDED()   //PIN_CLR(COM_GPIO1)
    #define COM_TASK_RESUMED()          //PIN_SET(COM_GPIO1)
    #define COM_TASK_SUSPENDED()        //PIN_CLR(COM_GPIO1)
    #define DEBUG_TASK_RESUMED()        //PIN_SET(COM_GPIO1)
    #define DEBUG_TASK_SUSPENDED()      //PIN_CLR(COM_GPIO1)
    #define GLORIA_START_IND()          //PIN_SET(COM_GPIO1)
    #define GLORIA_STOP_IND()           //PIN_CLR(COM_GPIO1)
    #define RADIO_TX_START_IND()        led_on(LED_SYSTEM)
    #define RADIO_TX_STOP_IND()         led_off(LED_SYSTEM)
    #define RADIO_RX_START_IND()        led_on(LED_SYSTEM)
    #define RADIO_RX_STOP_IND()         led_off(LED_SYSTEM)
  #endif /* FLOCKLAB */
#endif /* BASEBOARD */


/* --- STeC CONFIG --- */

/* base station */
#define BS_TASK_WAKEUP_PERIOD_MS        15000       /* period at which the base station should read from BOLT and handle time requests, in milliseconds */
#define BS_VALID_GENTIME_ABS_THRESHOLD  1e15        /* base station: generation time of received packets will be replaced if it is smaller than this threshold */
#define BS_VALID_GENTIME_REL_THRESHOLD  1e10        /* base station: generation time of received packets will be subtracted from the current time if it is smaller than this threshold */
#define BS_LORA_TX_POWER                14          /* transmit power for the base station */
#define BS_ACK_TIMESTAMP_OFS_US         190         /* time offset in us, added to the generation time of ACK messages on the base station to account for processing delay from get_time() until the TX starts */
/* general */
#define AGGREGATE_HEALTH_MESSAGES       0           /* if set to 0, nodes will send their health messages individually via LoRa to the base station; if set to 1, all nodes will wake up simultaneously to locally aggregate their health data before one of them sends it to the base */
#define CODETECTION_THRESHOLD           2           /* default value for codetection threshold (min. # nodes that need to detect the same event in order to report it) */
#define EVENT_TIMEOUT_ON_RETRIGGER      900         /* fail-safe mechanism: abort event on a re-trigger if it lasted longer than the specified value, in seconds */
#define ABORT_EVENT_ON_RTC_WAKEUP       0           /* set to 1 to prioritize RTC wake-ups (health message generation) to acquisition events (abort event on RTC wakeup) */
#define DISCOSYNC_NUM_SLOTS             9           /* number of slots for discosync (in case discosync is used for the staggered wakeup) */
/* staggered wakeup */
#define STAG_WAKEUP_MODE                WAKEUP_MODE_DISCOSYNC   /* possible values: wakeup_mode_t */
#define STAG_WAKEUP_TIMEOUT_MS          100                     /* staggered wakeup time */
#define STAG_WAKEUP_SLOT_MS             GLORIA_INTERFACE_FLOOD_DURATION_MS(GLORIA_INTERFACE_N_TX, NUM_HOPS, DPP_MSG_HDR_LEN + 2)  /* slot duration to send the wakeup message (payload is 0 bytes and CRC 2 bytes) */
/* radio config for staggered wakeup (if LoRa CAD mode is used) */
#define STAG_WAKEUP_LORA_BAND           GLORIA_INTERFACE_RF_BAND              /* radio band to use (use same as for FSK) */
#define STAG_WAKEUP_LORA_TX_POWER       10                                    /* transmit power in dBm, range [-9 - 22] */
#define STAG_WAKEUP_LORA_MODULATION     RADIO_LORA_SF_TO_MODULATION_INDEX(5)  /* LoRa modulation */
/* LoRa radio config for sending to the base station */
#if FLOCKLAB
  #define SEND_BS_LORA_BAND             19          /* radio band to use, 19 = ~865.4MHz (see table in radio_constants.c for options) */
#else  /* FLOCKLAB */
  #define SEND_BS_LORA_BAND             16          /* radio band to use, 16 = ~865MHz (see table in radio_constants.c for options) */
#endif /* FLOCKLAB */
#define SEND_BS_LORA_MODULATION         RADIO_LORA_SF_TO_MODULATION_INDEX(7)      /* LoRa modulation */
#define SEND_BS_LORA_TX_POWER           14                                        /* transmit power in dBm, range [-9 - 22] */
#define SEND_BS_LORA_FALLBACK_MODULATION  RADIO_LORA_SF_TO_MODULATION_INDEX(11)   /* LoRa modulation in case of a fallback */
#define SEND_BS_LORA_FALLBACK_BAND      48                                        /* spreading factor for LoRa in case of a fallback */
#define SEND_BS_RX_ACK_TIMEOUT_MS       (radio_get_toa(DPP_MSG_PKT_LEN, send_bs_modulation) / 1000 + 5)                 /* timeout value when no Ack from the base station is received. in ms */
#define SEND_BS_MIN_BACKOFF_MS          (2 * radio_get_toa(DPP_MSG_PKT_LEN, send_bs_modulation) / 1000)                 /* minimum backoff time if the channel is occupied, in ms */
#define SEND_BS_MIN_BACKOFF_WC_MS       (2 * radio_get_toa(DPP_MSG_PKT_LEN, SEND_BS_LORA_FALLBACK_MODULATION) / 1000)   /* worst case minimum backoff time if the channel is occupied, in ms */
#define SEND_BS_RAND_BACKOFF_MS         (100)       /* maximum random backoff value, in ms */
#define SEND_BS_TRIES                   3           /* how many times the leader tries to send the message to the base station */
#define SEND_BS_FALLBACK_THRESHOLD      5           /* number of times a transmission to base has to fail (in a row) before switching to the fallback mode 1 radio config */
#define SEND_BS_FALLBACK_2_THRESHOLD    10          /* number of times a transmission to base has to fail (in a row) before switching to the fallback mode 2 radio config (max TX power) */
#define SEND_BS_TIMESTAMP_OFS_US        280         /* time offset in us to account for processing delay, used to adjust the relative generation times in send_to_basestation() */
#define SEND_BS_CHANNEL_FREE_RSSI_TH    0           /* RSSI threshold in dBm (negative value) for channel free detection; set to zero to use CAD instead of RSSI */
/* leader election */
#define LEADER_ELECTION_SLOT_TIME_MS    (GLORIA_INTERFACE_FLOOD_DURATION_MS(GLORIA_INTERFACE_N_TX, NUM_HOPS, 2) + 10)   /* Gloria slot duration for the leader election phase */
#define LEADER_ELECTION_GAP_TIME_MS     10
#define LEADER_ELECTION_MODE            LEADER_ELECTION_MODE_BINARY_SEARCH  /* default leader election mode */
#define LEADER_ELECTION_OFFSET_MS       (STAG_WAKEUP_SLOT_MS + 10)          /* time between sending the wake-up message and start of the leader election, in ms. */
#define LEADER_ELECTION_ALIGN_SLOTS     1                                   /* whether to align the communication slots during leader election (only works if discosync is used) */
/* data aggregation */
#define DATA_AGGREGATION_DELAY_MS       15000       /* how long to wait before starting data aggregation (don't forget to also adjust this setting on the APP/geophone side), must be larger than (LEADER_ELECTION_OFFSET_MS + DATA_AGGREGATION_GUARD_TIME_MS) */
#define DATA_AGGREGATION_GUARD_TIME_MS  2           /* RX guard time for the schedule slot at the beginning of the data aggregation */
#define DATA_AGGREGATION_SLOT_TIME_MS   GLORIA_INTERFACE_FLOOD_DURATION_MS(GLORIA_INTERFACE_N_TX, NUM_HOPS, DPP_MSG_PKT_LEN)    /* duration of Gloria flood during the data aggregation and ACK distribution phase, in ms */
#define DATA_AGGREGATION_GAP_TIME_MS    5                                                                                       /* spacing between two Gloria floods during the data aggregation and ACK distribution phase, in ms */
/* ACK / command distribution */
#define DISTR_ACKCMD_DELAY_MS           ((DATA_AGGREGATION_SLOT_TIME_MS + DATA_AGGREGATION_GAP_TIME_MS) * (MAX_NUM_NODES + 1) + SEND_BS_TRIES * (SEND_BS_MIN_BACKOFF_WC_MS + SEND_BS_RAND_BACKOFF_MS + 10))    /* Time between the sync point and distribution of the ACK / commands */
#define DISTR_ACKCMD_CONT_SLOT          1           /* whether to append a contention slot after the ACK distribution */
#define DISTR_ACKCMD_TIMESTAMP_OFS_US   40          /* offset (in us) added to the timestamp by the reporter before ACK distribution */


/* --- parameter checks --- */

#if BASEBOARD_TREQ_WATCHDOG > 0 && BASEBOARD_TREQ_WATCHDOG < 120
#error "BASEBOARD_TREQ_WATCHDOG must be >= 120"
#endif

#if NODE_HEALTH_MSG_INTERVAL > 1440 || (!AGGREGATE_HEALTH_MESSAGES && (NODE_HEALTH_MSG_INTERVAL > 0) && (NODE_HEALTH_MSG_INTERVAL <= ((MAX_NUM_NODES * NODE_HEALTH_REPORTING_OFS_S) / 60)))
#error "NODE_HEALTH_MSG_INTERVAL must be between 0 and 1440"
#endif

#if !BASESTATION && !FLOCKLAB && (NODE_ID != FLOCKLAB_NODE_ID) && (NODE_ID >= (MIN_NODE_ID + MAX_NUM_NODES) || NODE_ID < MIN_NODE_ID)
#error "NODE_ID is outside of [MIN_NODE_ID, MIN_NODE_ID + MAX_NUM_NODES]"
#endif

#if BASESTATION && !FLOCKLAB && (BS_MIN_NODE_ID > MIN_NODE_ID || BS_MAX_NODE_ID < (MIN_NODE_ID + MAX_NUM_NODES))
#error "invalid config for BS_MIN_NODE_ID or BS_MAX_NUM_NODES"
#endif

#if FW_OTA_ENABLE && FW_OTA_MASTER && (LORA_TX_QUEUE_SIZE < 2)
#error "LoRa TX queue size must be at least 2"
#endif

#if CODETECTION_THRESHOLD > 8
#error "CODETECTION_THRESHOLD must be <= 8"  /* only 3 bits (values 1..8) are reserved in the health_min message for this value */
#endif

#endif /* __APP_CONFIG_H */
