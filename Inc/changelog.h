/*
 * Copyright (c) 2020 - 2021, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * STeC changelog
 */

/*

v1.0.1:
- issue with canceling the RTC wakeup fixed
- debug prints for command embedding on basestation adjusted
- issue fixed where baseboard check was skipped if BASEBOARD was set to 0

v1.0.0:
- spreading factor in fallback mode changed to SF11
- refactoring of message handling code
- minor fix in FW OTA update
- health reporting offset increased to 12s
- timestamp offsets adjusted

v0.5.3:
- bugfix in FW OTA update
- minor change in BOLT driver
- minor adjustment to task stack size
- refactoring of message handling code

v0.5.2:
- code refactoring, lib updated

v0.5.1:
- issue fixed where the transmission start was shifted by 10us in some cases
- minor adjustments to discosync

v0.5.0:
- print actual drift instead of the uncompensated drift
- use discosync as default staggered wakeup mode
- bugfix in reporter assignment if wrong leader was detected
- collect max hop count instead of average
- use radio timeout on basestation to periodically restart RX mode (to mitigate the RX issue)
- slots in leader election aligned
- flora lib updated (includes adjusted radio drivers)

v0.4.19:
- time offsets adjusted for more accurate timestamping
- slot alignment in data aggregation round improved
- sx1262 bugfix (perform image calibration after cold sleep and POR)

v0.4.18:
- Gloria interface: use the same preamble length for transmitter and receiver and only set the radio config once at the beginning of the flood
- potential issue in enqueue_message() fixed
- major bugfix in BOLT access that caused spurious CRC errors (SPI clock speed out of spec)
- config added to set a default wakeup time for the periodic baseboard enable

v0.4.17:
- RTC alarm for health message generation is only updated if it is in the past
- issue fixed with TIME_UPDATED event generation
- issue fixed with invalid generation time of node info and reset event
- include modulation in config field of the health message
- reset event value fixed

v0.4.16:
- threshold parameter added for "TIME_UPDATED" event to reduce the number of generated event messages
- messages of unknown type are not forwarded
- messages of type FW targeted at a node that doesn't have the FW update feature enabled should only print a warning but not generate an event message
- do not generate a warning event when message of type MIN received
- instead of using a busy-wait delay after the data aggregation, use a state variable to skip the first transmission attempt to the base station
- LPM_MIN_IDLE_TIME_MS increased from 10 to 20ms
- timesync task stack size increased
- base station: issue fixed with the time synchronization for the case where more than 1 timesync message is waiting in the BOLT queue

v0.4.15:
- command added to adjust the health message period
- bugfix in event bit setting for health message
- don't allow broadcast of message SET_NODE_ID
- minor bugfix in fw_ota

v0.4.14:
- code review comments implemented
- treat DPP messages with payload length zero as valid
- also check timesync task stack watermark in rtos_check_stack_usage()

v0.4.13:
- update generation time of all packets before transmission
- change argument for SET_TX_POWER command from 8-bit to 16-bit
- EVENT_SX1262_TIME_UPDATED event added
- always use RTC as initial timestamp (don't use compile time)
- base station: replace generation time if it appears to be invalid
- don't include component ID when adding the command type to an event value
- only write config back to flash memory if it has changed

v0.4.12:
- health min extended: include node config and events
- upper limit for codetection threshold reduced to 8
- base station: use correct lora modulation to calculate the TOA / packet receive timestamp
- base station: generate an event when a command has been embedded into an ACK_CMD message
- base station: print the configured transmit power when task starts

v0.4.11:
- bugfix: command forwarding on base station
- base station: print correct modulation and band on startup
- base station: sniffer should not send any messages
- base station: do not enable FW OTA feature on sniffer nodes
- reporter ID selection improved (more uniform selection)

v0.4.10:
- load_config: handling to determine node ID changed (prefer ID in flash memory)
- use config.node_id instead of NODE_ID
- command added to set the node ID
- base station: read up to 100 messages at once from bolt
- log buffer size increased

v0.4.9:
- base station: generate node info packet at startup
- base station: update local_timestamp in set_time() function
- new parameters added: BASESTATION_FALLBACK and BASESTATION_SNIFFER (note: fallback base is not compatible with source nodes of version 0.3.x)
- new field 'config' added to node info to include the chosen compile-time parameters
- base station: do not keep LED on in receive mode to mitigate the wear, instead blink LED in task
- also reset radio if reset command received
- minor bugfix in FW OTA updater

v0.4.8:
- base station: don't send an ACK for unknown/unexpected message types
- allow the base station to have a larger queue size than source nodes
- (experimental) FW OTA update functionality added
- allow up to +22dBm TX power
- new command added to configure the staggered wakeup timeout
- allow to change the TX power for the fallback mode and parameter SEND_BS_LORA_FALLBACK_TX_POWER removed
- fallback mode 2 with respective configurable threshold added where TX power is increased to the max (+22dBm)

v0.4.7:
- option added to prioritize health message generation over acquisition events (ABORT_EVENT_ON_RTC_WAKEUP)
- radio wakeup is now handled in low-power manager
- support to send DPP min-type messages added
- base station: now supports back-to-back transmission of several packets, and make sure an ongoing transmission is not aborted

v0.4.6:
- set TX power command: also set power for sending packets to the base station (except for fallback)
- EVENT level set to INFO, and for command also include the argument in the event code
- do not overwrite com task notification value for RTC wakeup
- wake baseboard threshold for bolt write integrated into flora lib

v0.4.5:
- message handling adjusted to avoid potential loops
- handling of regular command messages removed for source nodes (handle only ACK_CMD)

v0.4.4:
- base station: ensure that there is at least 1s gap between clearing and setting the baseboard enable pin (TREQ watchdog)
- base station: only forward commands for other nodes via LoRa if the component ID is SX1262
- baseboard: write threshold added to enable the baseboard in time to avoid an overflowing BOLT queue
- count the number of dropped packets due to BOLT queue full and generate an event as soon as there is space in the BOLT queue
- buffer overflow issue of serial logging fixed

v0.4.3:
- base station: non-broadcast commands not targeted at a source node in the network will be added to the LoRa TX queue
- save config if updated via command

v0.4.2:
- compile date (__TIMESTAMP__) was last-modified date of main.c -> replaced by __DATE__ and __TIME__
- print network size and node ID range
- base station: range of possible node IDs can be larger than on a source node
- base station: now accepts baseboard enable/disable commands
- corrupted flash memory detection and recovery added (recovery only possible if it was caused by nvcfg)

v0.4.1:
- command added to enable / disable CAD (listen before send)
- base station: do not cancel and restart receive mode if already in RX
- base station: if the target ID of a received message is 0, replace it with the node ID
- base station: before replacing the generation time, make sure the message CRC is correct
- base station: sniffer does not send an ACK, but forwards packets to BOLT
- issue with RSSI sampling fixed

v0.4.0:
- issue with fallback radio config fixed (use correct band and TX power)
- check added to avoid message processing in interrupt context and events that are potentially generated within an ISR removed
- minor improvements to debug printing
- generate an event if TX to base failed
- functionality added to perform continuous CAD, RSSI or packet sniffing on a base station

v0.3.8 (used for first deployment):
- make sure app health data is valid before embedding it into health_min
- revert previous change, go back to CAD for channel-free detection
- generate an event message after a reset

v0.3.7:
- schedule node health transmissions further apart of each other (6s window per node to account for SF10 and retransmissions)
- go to LPM while waiting for the retransmission (send to base)
- time conversion error fixed for generation time in send to base
- for SF8 or higher: use RSSI-based method to determine if channel is free

v0.3.6:
- issue with the re-trigger in combination with the timesync task fixed
- MSG_IGNORED event logging fixed

v0.3.5:
- more event logging added
- use a variable to store the current lora modulation
- use worst case modulation (SF10) for the ACK distribution delay calculation (DISTR_ACKCMD_DELAY_MS)
- LPM_MIN_IDLE_TIME_MS define added
- go back to LPM on a retrigger if in a WAIT state

v0.3.4:
- only accept APP health if device ID matches
- minor improvements

v0.3.3:
- clear all messages from the BOLT / FSK TX queue when a new event starts
- base station: set target ID for commands
- issue fixed where broadcast messages were not properly processed by the base station

v0.3.2:
- issue fixed where a node would not process the commands embedded in the ACK if it is alone (e.g. for health reporting)
- base station: embed commands also into ACKs for other message types
- base station: support for broadcast commands added
- only accept messages of type HEALTH_MIN and GEO_ACQ in data aggregation and accept ACK_CMD only during ACK distribution
- clear payload buffer if Gloria is started as a receiver
- trigger a reset after 30s if Error_Handler is entered
- throw BOLT_ERROR instead of generic QUEUE_FULL if write fails

v0.3.1:
- DISTR_ACKCMD_DELAY_MS and SEND_BS_RX_ACK_TIMEOUT_MS adjusted
- for staggered wakeup: restart Gloria RX if wrong wakeup message received and still enough time
- potential infinite loop issue fixed with bolt queue full and the resulting event generation

v0.3.0:
- max. number of nodes set to 16
- EVENT_TIMEOUT_ON_RETRIGGER increased to 15min
- SEND_BS_RX_ACK_TIMEOUT_MS now depends on the max. TOA
- check for event timeout on an RTC trigger
- formula for DISTR_ACKCMD_DELAY_MS adjusted
- SEND_BS_MIN_RAND_BACKOFF_MS and SEND_BS_MAX_RAND_BACKOFF_MS replaced by SEND_BS_MIN_BACKOFF_MS and SEND_BS_RAND_BACKOFF_MS
- random delay added before sending a packet to the base station, and delay if channel occupied set to SEND_BS_MIN_BACKOFF_MS
- no backoff on the last retransmission-to-base try
- bugfix: do not enter SEND_TO_BS state if lora_tx queue is empty
- bugfix in RTC drift compensation
- option added to use CAD instead of RSSI threshold to determine whether the channel is free
- only do 'channel free' check once before sending to base
- check the slot counter during the leader election, ignore packets with an invalid (unexpected) counter value

v0.2.11:
- overwrite com task notification value on 'continue' and 'abort', but do not overwrite it on a trigger (otherwise a re-trigger might freeze the state machine)
- do not enter stop mode if the RTC semaphore is taken
- error handling in lptimer expiration adjusted

v0.2.10:
- timestamp request timeout added as a fail-safe mechanism (rather don't send a timestamp than a wrong one)
- default TX power for FSK set to +14 dBm
- for health messages: keep the smaller uptime of the two components

v0.2.9:
- event timeout as a fail-safe mechanism added (EVENT_TIMEOUT_ON_RETRIGGER)
- issue fixed: don't handle state machine on a retrigger

v0.2.8
- yet another attempt to fix the buggy lptimer

v0.2.7
- lptimer adjusted
- timestamp capture accuracy improved
- issue fixed that was introduced with previous version

v0.2.6
- issue fixed where the staggered wakeup would be restarted in case an invalid wakeup packet was received
- potential issue with BOLT init fixed (startup delay required)
- error handling improved on base station when a timestamp is received but no TREQ capture occurred
- do not reconfig the TREQ pin as GPIO interrupt on the base station
- new commands added: CMD_SX1262_SET_TX_POWER, CMD_SX1262_SET_CODETECTION_THRESHOLD
- RX guard time for data aggregation schedule slot added (DATA_AGGREGATION_GUARD_TIME_MS)

v0.2.5
- timesync accuracy between base and source nodes improved
- SEND_BS_MIN_RAND_BACKOFF_MS parameter added (calculated based on average packet TOA)
- bugfix: nodes must receive the packet in the leader election to extract the slot number glora_get_rx_started_cnt() is not sufficient
- bugfix: do not poll the com task when entering random backoff due to channel activity
- flora lib updated

v0.2.4
- support for COM_GPIO2 trigger interrupt dropped due to compatibility issues in conjunction with the baseboard, default pin config is now SWO
- signal amplitude conversion issue fixed
- get_time() on base station moved from ISR to task

v0.2.3
- drift is not updated if it exceeds the max. specified drift
- issue fixed where the node's own message would be inserted into the aggregated message twice
- minor issues with data conversion/aggregation and max stack wm calculation fixed
- minor changes to parameters

v0.2.2
- toggle INT2 pin on base station upon packet reception to map local to global time on FlockLab
- commas removed from debug strings
- health message interval increased to 1h
- flush BOLT queue at startup

v0.2.1
- in STAG_WAKEUP_RX_MODE 'always RX', the staggered wakeup will now be aborted as soon as the Glora flood terminates (removes the requirement LEADER_ELECTION_OFFSET_MS > STAG_WAKEUP_TIMEOUT_MS)
- before reporting the ACK, a random wait time between 1 and 10 ms is added to reduce the probability of a collision in case two nodes think they are reporters
- reporter ID is now chosen in the range [MIN_NODE_ID..LEADER_ID]
- print expected leader ID after leader election
- bugfix in lptimer (don't change IER when timer is enabled)
- leave GPIO clocks enabled in LPM in order to trace interrupts (current drain does not seem to be affected significantly)

v0.2.0
- check sender ID of each received packet during data collection
- 'new leader' renamed to reporter, leader will not change after the LE
- option SEND_BS_DELEGATE_LEADER removed
- if multiple leaders are detected, the wrong leader will back down but also report the aggregated data to the base
- delay before data aggregation now includes the leader election
- if no ACK is received from the base station, the reporter nevertheless participates in the ACK distribution round (without sending anything)
- go to LPM while waiting for leader election to start
- use semaphore to lock RTC access instead of using a busy-wait loop to stall execution until RTC shift is complete (required new com task notification)
- use FLOCKLAB_IND2 pin to show the individual phases of the protocol (wakeup, leader election, data aggregation, sent to base and ack distribution)
- gap time for data collection reduced to 5ms
- TX power for FSK increased to +10dBm due to excessive packet loss on FlockLab
- issue fixed with reporter identification (unsigned int for negative difference)
- baudrate reduced to 460800 for FlockLab to avoid corrupted output

v0.1.12
- parameter SEND_BS_TIME_OFS_US added
- time on air taken into consideration when calculating the timestamp received with an ACK message
- option added to disable data aggregation for health messages (and instead send those messages directly/individually to the base)

v0.1.11
- use predefined LoRa modulations from radio_constants.c instead of custom config
- subtract TOA when base station sets the timestamp of a received message
- transmit power for FSK reduced to 0dBm and for staggered wakeup (LoRa) to +10dBm
- changes to function analyze_aggr_message() (used to determine whether an aggregated message is of interest)

v0.1.10
- boosted RX enabled for source nodes for receiving the ACK from the base
- forward commands not intended for this component ID to BOLT
- separate Gloria slot time parameter for leader election
- Gloria slot time are calculated based on the number of hops/TX and the max. packet length
- print warning if not all data fits into one aggregated message
- do not overwrite the captured TREQ timestamp if a request is already pending
- calculation of event start time adjusted
- a leader node can detect if there is another node in the network with a higher ID (and thus the actual leader)
- replace generation time of aggregated geo acq data with relative offset before sending the packet -> base station will set the generation time
- event logging level set to warning, target set to BOLT, and more events added
- schedule the ACK distribution round based on the reference time

v0.1.9
- several bugfixes
- use different LoRa channel on FlockLab to send to base station
- leader election offset increased
- issue with misaligned RTC alarms fixed
- use LPM during the wait states (wait for data aggregation and wait for ack)

v0.1.8
- send aggregated message to the application processor for logging
- do not disable time request interrupt when woken from LPM
- if a trigger occurs during data aggregation or later, another data aggregation round will be scheduled (contention slot after ACK distribution)
- issue fixed in message aggregation routine

v0.1.7
- node list is static -> no need to send it in a schedule for data collection
- leader can now delegate another node to send the aggregated data home
- bolt read queue removed (instead, messages are processed directly and a copy of the last app health message is saved)
- timestamp request is answered after the resync with the base station
- fallback radio config added
- LoRa radio channel set to 16, FSK to 43, and some other parameters adjusted
- cleanup and compilation errors fixed

v0.1.6
- timesync task added, drift compensation adjusted
- RTC drift compensation added
- geo acq start time relative to the reference time instead of absolute time
- configurable node health message interval in minutes
- the required delay (meeting point) for ACK distribution is now calculated based on the selected parameters
- base station RX mode changed to "boosted" for better sensitivity
- check if channel free check changed (no more exponential random backoff)
- all nodes do data aggregation and decide whether the event is of interest
- some parameters adjusted and unused parameters removed

v0.1.5
- health_min message extended and radio stats added
- parameters adjusted
- function added to set the RTC time in milliseconds and bugfix in RTC set timestamp
- issue fixed where long sync intervals lead to invalid drift estimation due to an overflow
- minor adjustments to message structures and data aggregation

v0.1.4
- code runs on FlockLab 2
- BOLT task is created only if BOLT_ENABLE is set (i.e. no BOLT task on FlockLab)
- misc fixes (timing, slot alignment and spacing, missing state machine transitions)
- max. number of nodes set to 16, gloria slot time reduced to 20ms, TX power for glora floods increased to +10dBm
- new config parameters BASESTATION and SEND_BS_ACK_TIMESYNC_OFS added, and minor app_config cleanup

v0.1.3
- issue with RTC fixed
- new parameter COM_GPIO2_TRIGGER added
- debug prints adjusted
- task stack size increased
- state machine extended (ACK + command distribution / processing)
- minor cleanup in message handling

v0.1.2
- use RX timeout on base station to resume BOLT and debug tasks
- fixed timestamp request feature fixed and simplified (use hs timer on base station and lptimer on source nodes, configuration parameter TIMESTAMP_USE_HS_TIMER removed)
- issue with COM_TREQ interrupt handling fixed
- issue with data collection fixed
- IRQ mode for radio set to ALL
- RTC trigger added

v0.1.1
- new data structures added for aggregated health and acquisition messages
- base station can now store one command per node and embeds commands into an ACK message
- some issues fixed

v0.1.0
- restructuring of code base finished

v0.0.1
- initial commit (code based on eLWB and event-triggered communication MA tobiasga)


 */
