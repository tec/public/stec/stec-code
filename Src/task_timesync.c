/*
 * Copyright (c) 2020 - 2021, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * handles the time synchronization
 */

#include "main.h"


/* Global variables ----------------------------------------------------------*/

extern TaskHandle_t      xTaskHandle_timesync;
extern SemaphoreHandle_t xSemHandle_rtc;
extern TIM_HandleTypeDef htim2;


/* Private define ------------------------------------------------------------*/

#ifndef TIMESYNC_TASK_RESUMED
#define TIMESYNC_TASK_RESUMED()
#define TIMESYNC_TASK_SUSPENDED()
#endif /* TIMESYNC_TASK_RESUMED */


/* Private variables and functions -------------------------------------------*/

static uint64_t unix_timestamp_us        = 0;        /* UNIX timestamp of the last sync point, in us */
static uint64_t local_timestamp_ticks    = 0;        /* local timestamp of the last sync point, in timer ticks (source node: lptimer, base station: hs timer) */
static uint64_t captured_timestamp_ticks = 0;        /* captured timestamp of the last time request (COM_TREQ rising edge) in lptimer ticks -> only used on source nodes */
static int32_t  average_drift_ppm        = 0;        /* average clock drift of the local time against the reference time, in ppm */
static bool     timestamp_updated        = false;
#if !BASESTATION
static bool     timestamp_requested      = false;
#endif /* BASESTATION */


/* Functions -----------------------------------------------------------------*/


static void init_time(void)
{
  /* load timestamp from RTC */
  unix_timestamp_us     = rtc_get_unix_timestamp_ms() * 1000;
  local_timestamp_ticks = 0;
  LOG_INFO("UNIX timestamp %llu loaded from RTC", unix_timestamp_us);
}


static void update_time(void)
{
  static uint64_t prev_local_timestamp_ticks = 0;
  static uint64_t prev_unix_timestamp_us     = 0;

  if (timestamp_updated) {
    /* calculate the drift */
    if (prev_unix_timestamp_us) {
      int64_t  unix_ts_diff_us     = (int64_t)(unix_timestamp_us - prev_unix_timestamp_us);
      uint64_t local_ts_diff_ticks = (uint64_t)(local_timestamp_ticks - prev_local_timestamp_ticks);
  #if BASESTATION
      /* the base station uses the high-speed oscillator for time synchronization (8MHz) */
      int64_t local_ts_diff_us     = (local_ts_diff_ticks * 1000000ULL / HS_TIMER_FREQUENCY);
      int64_t local_ts_diff_eff_us = (local_ts_diff_ticks * (1000000ULL - average_drift_ppm) / HS_TIMER_FREQUENCY);   // effective difference with drift compensation applied
  #else /* BASESTATION */
      /* source nodes use the low-frequency oscillator for time synchronization (32kHz) */
      int64_t local_ts_diff_us     = (int64_t)(local_ts_diff_ticks * 1000000ULL / LPTIMER_SECOND);
      int64_t local_ts_diff_eff_us = (int64_t)(local_ts_diff_ticks * (1000000ULL - average_drift_ppm) / LPTIMER_SECOND);  // effective difference with drift compensation applied
  #endif /* BASESTATION */
      int32_t drift_ppm     = (int32_t)((int64_t)(local_ts_diff_us - unix_ts_diff_us) * 1000000LL / unix_ts_diff_us);
      int32_t drift_eff_ppm = (int32_t)((int64_t)(local_ts_diff_eff_us - unix_ts_diff_us) * 1000000LL / unix_ts_diff_us);
      /* drift has to be within a certain range */
      if (drift_ppm < TIMESTAMP_MAX_DRIFT && drift_ppm > -TIMESTAMP_MAX_DRIFT) {
        if (drift_ppm > TIMESTAMP_TYPICAL_DRIFT || drift_ppm < -TIMESTAMP_TYPICAL_DRIFT) {
          LOG_WARNING("drift is larger than usual");
        }
        if (average_drift_ppm == 0) {
          average_drift_ppm = drift_ppm;
        } else {
          average_drift_ppm = (average_drift_ppm + drift_ppm) / 2;
        }
        /* note: a negative drift means the local time runs too slow */
        LOG_INFO("current drift: %ldppm   drift compensation: %ldppm ", drift_eff_ppm, average_drift_ppm);

      } else {
        LOG_WARNING("drift is too large (%ldppm)", drift_ppm);
        EVENT_WARNING(EVENT_SX1262_TSYNC_DRIFT, (uint32_t)drift_ppm);
      }
    }

    prev_local_timestamp_ticks = local_timestamp_ticks;
    prev_unix_timestamp_us     = unix_timestamp_us;

    /* update the RTC */
    uint32_t waittime_ms = 0;
    if (xSemaphoreTake(xSemHandle_rtc, 0)) {     /* lock RTC access (don't wait since semaphore must always be available at this point) */
      uint64_t rtc_time_ms  = rtc_get_unix_timestamp_ms();
      uint64_t curr_time_ms = get_time(0) / 1000;
      if (rtc_set_unix_timestamp_ms(curr_time_ms, &waittime_ms)) {
        if (waittime_ms) {
          int32_t time_delta_ms = (int32_t)(curr_time_ms - rtc_time_ms);
          if (ABS(time_delta_ms) > TIMESTAMP_OFS_WARN_THRESHOLD_MS) {
            EVENT_INFO(EVENT_SX1262_TIME_UPDATED, time_delta_ms);
          }
          vTaskDelay(pdMS_TO_TICKS(waittime_ms));
        }
      } else {
        LOG_WARNING("failed to set RTC timestamp");
        EVENT_WARNING(EVENT_SX1262_RTC_ERROR, 1);
      }
      xSemaphoreGive(xSemHandle_rtc);           /* release RTC access */
    } else {
      LOG_ERROR("failed to lock the RTC (time not updated)");      /* this is not supposed to happen */
      EVENT_ERROR(EVENT_SX1262_MUTEX_ERROR, 0);
    }
  #if !BASESTATION
    /* set drift compensation (since RTC uses the same clock source as the lptimer, we can use the estimated drift) */
    rtc_compensate_drift(-average_drift_ppm);
  #endif /* BASESTATION */
  }
  timestamp_updated = false;
}


#if BASESTATION

bool handle_treq(void)
{
  /* timer 2 capture compare flag 4 set? (COM_TREQ) */
  if (__HAL_TIM_GET_FLAG(&htim2, TIM_FLAG_CC4)) {
    uint64_t curr_timestamp_hsticks = hs_timer_now();
    uint32_t elapsed_ticks          = (uint32_t)curr_timestamp_hsticks - htim2.Instance->CCR4;
    captured_timestamp_ticks        = curr_timestamp_hsticks - elapsed_ticks;
    LOG_VERBOSE("TREQ pin toggled %lums ago", (elapsed_ticks / (HS_TIMER_FREQUENCY / 1000)));

    __HAL_TIM_CLEAR_FLAG(&htim2, TIM_FLAG_CC4);      /* clear capture compare interrupt flag */
    __HAL_TIM_CLEAR_FLAG(&htim2, TIM_FLAG_CC4OF);    /* clear capture overrun flag */

    return true;
  }
  return false;
}

#endif /* BASESTATION */


/* set the time based on a given UNIX timestamp and hstimer ticks timestamp pair */
void set_time(uint64_t unix_time_us, uint64_t timestamp_hsticks, bool notify_tsynctask)
{
#if !FLOCKLAB
  if (unix_time_us < 1000000ULL * BUILD_TIME) {
    LOG_WARNING("invalid timestamp (time update skipped)");
    return;
  }
#endif /* FLOCKLAB */

#if BASESTATION

  if (timestamp_hsticks) {
    local_timestamp_ticks = timestamp_hsticks;
    unix_timestamp_us     = unix_time_us;
    timestamp_updated     = true;
  } else {
    if (handle_treq() || timestamp_updated) {
      local_timestamp_ticks = captured_timestamp_ticks;
      unix_timestamp_us     = unix_time_us;
      timestamp_updated     = true;
    }
  }

#else /* BASESTATION */

  uint64_t curr_lpticks,
           curr_hsticks;
  /* convert to lptimer ticks */
  if (lptimer_now_synced(&curr_lpticks, &curr_hsticks)) {
    local_timestamp_ticks = curr_lpticks - (((int64_t)curr_hsticks - (int64_t)timestamp_hsticks) * LPTIMER_SECOND / HS_TIMER_FREQUENCY);
    unix_timestamp_us     = unix_time_us;
    timestamp_updated     = true;
  } else {
    LOG_ERROR("failed to get synced timestamps");
    return;
  }

#endif /* BASESTATION */

  if (notify_tsynctask) {
    // notify the timesync task
    if (IS_INTERRUPT()) {
      vTaskNotifyGiveFromISR(xTaskHandle_timesync, 0);
    } else {
      xTaskNotifyGive(xTaskHandle_timesync);
    }
  }
}


uint64_t get_time(uint64_t at_time)
{
#if BASESTATION
  /* on the base station, the hs timer runs all the time -> use it for better accuracy */
  if (at_time == 0) {
    at_time = hs_timer_now();
    if (at_time == 0) {
      LOG_WARNING("invalid timestamp");
      return 0;
    }
  }
  return (uint64_t)((int64_t)unix_timestamp_us + (((int64_t)at_time - (int64_t)local_timestamp_ticks) * (1000000LL - average_drift_ppm) / HS_TIMER_FREQUENCY));
#else /* BASESTATION */
  if (at_time == 0) {
    at_time = lptimer_now();
  }
  return (uint64_t)((int64_t)unix_timestamp_us + (((int64_t)at_time - (int64_t)local_timestamp_ticks) * (1000000LL - average_drift_ppm) / LPTIMER_SECOND));
#endif /* BASESTATION */
}


/* returns an estimate for the current (averaged) drift in ppm */
int32_t get_drift(void)
{
  return average_drift_ppm;
}


int32_t get_drift_comp_us(uint32_t elapsed_time_us)
{
  return elapsed_time_us * average_drift_ppm / 1000000;
}


uint64_t get_captured_timestamp(void)
{
  return captured_timestamp_ticks;
}


bool is_rtc_ready(void)
{
  /* make sure the RTC semaphore is free before continuing to enter stop mode 2 */
  if (xSemaphoreTake(xSemHandle_rtc, 0) != pdTRUE) {
    return false;
  }
  xSemaphoreGive(xSemHandle_rtc);
  return true;
}


#if !BASESTATION

void GPIO_PIN_3_Callback(void)
{
  if (!timestamp_requested) {
    /* only overwrite if there is not already a pending timestamp request */
    captured_timestamp_ticks = lptimer_now() - 1;   /* subtract wakeup + ISR + function call delays (measured to ~20us) */
    timestamp_requested = true;
  }

  /* restore all clocks and peripherals */
  lpm_update_opmode(OP_MODE_EVT_WAKEUP);

  LOG_VERBOSE("timestamp request (%llu)", get_time(captured_timestamp_ticks));

  /* send notification to the communication task */
  notify_com_task(COM_TASK_NOTIFY_TRIGGER, false, true);    // do not overwrite pending notification value
}

#elif FLOCKLAB

void GPIO_PIN_3_Callback(void)
{
  /* print the timestamp */
  LOG_VERBOSE("event trigger timestamp: %llu", get_time(__HAL_TIM_GET_COUNTER(&htim2)));
}

#endif /* BASESTATION */


#if FLOCKLAB

/* on FlockLab, the trigger will come in via the SIG1 pin */
void GPIO_SIG1_Callback(void)
{
  GPIO_PIN_3_Callback();
}

#endif /* FLOCKLAB */


void task_timesync(void const * argument)
{
  LOG_VERBOSE("timesync task started");

  /* initialize the UNIX timestamp */
  init_time();

#if BASESTATION
  /* configure input capture for TIM2_CH4 (PA3, TREQ pin) */
  HAL_TIM_IC_Start(&htim2, TIM_CHANNEL_4);
#endif /* BASESTATION */

  /* Infinite loop */
  for (;;)
  {
    /* wait for notification token (= explicitly granted permission to run) */
    TIMESYNC_TASK_SUSPENDED();
    ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
    TIMESYNC_TASK_RESUMED();

  #if BASESTATION
    /* base station only: handle captured hs timer timestamp and update the time */
    handle_treq();
    update_time();
  #else /* BASESTATION */
    update_time();
    if (timestamp_requested) {
      /* only send the timestamp if the request is no more than TIMESTAMP_REQUEST_TIMEOUT seconds ago */
    #if EVENT_TIMEOUT_ON_RETRIGGER
      if (LPTIMER_TICKS_TO_S(lptimer_now() - captured_timestamp_ticks) < TIMESTAMP_REQUEST_TIMEOUT)
    #endif /* EVENT_TIMEOUT_ON_RETRIGGER */
      {
        generate_timestamp(captured_timestamp_ticks);
      }
      timestamp_requested = false;
    }
  #endif /* BASESTATION */

    //LOG_VERBOSE("timesync task executed");
  }
}

