/*
 * Copyright (c) 2020 - 2021, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * DPP message processing
 */

#include "main.h"


extern QueueHandle_t xQueueHandle_fsk_rx;
extern QueueHandle_t xQueueHandle_fsk_tx;
extern QueueHandle_t xQueueHandle_lora_tx;
extern TaskHandle_t  xTaskHandle_basestation;
extern bool          abort_event_on_rtc_trigger;
extern int8_t        send_bs_tx_pwr;
extern bool          send_bs_cad_mode;
extern uint32_t      send_bs_fail_cnt;
extern uint16_t      stag_wakeup_timeout_ms;

uint16_t health_msg_period = NODE_HEALTH_MSG_INTERVAL;
uint8_t  codetection_th    = CODETECTION_THRESHOLD;

#if FW_OTA_ENABLE
bool   fw_update_mode = false;
#endif /* FW_OTA_ENABLE */


/* Private variables ---------------------------------------------------------*/

static dpp_message_t      msg_buffer;
static dpp_app_health_t   last_app_health;
static event_msg_level_t  event_msg_level  = EVENT_MSG_LEVEL;
static event_msg_target_t event_msg_target = EVENT_MSG_TARGET;
static bool               app_health_rcvd  = false;
static uint16_t           detected_events  = 0;

#if BASESTATION
static uint32_t pending_cmds[BS_MAX_NODE_ID - BS_MIN_NODE_ID + 1];        // allow 1 pending command per node ID (use node ID as index)
#endif /* BASESTATION */

#if BASEBOARD
LIST_CREATE(pending_bb_cmds, sizeof(scheduled_cmd_t), BASEBOARD_CMD_QUEUE_SIZE);    // list to store the pending baseboard enable/disable commands
#endif /* BASEBOARD */


/* Functions -----------------------------------------------------------------*/

bool process_command(const dpp_command_t* cmd, const dpp_header_t* hdr)
{
  bool cfg_changed = false;
#if BASEBOARD
  scheduled_cmd_t sched_cmd;
#endif /* BASEBOARD */

  if (!cmd) {
    return false;
  }

  switch (cmd->type) {

  case DPP_COMMAND_RESET:
  case CMD_SX1262_RESET:
    radio_reset();
    NVIC_SystemReset();
    break;

  case CMD_SX1262_SET_NODE_ID:
    if (node_id_valid(cmd->arg16[0])) {
      config.node_id = cmd->arg16[0];
      LOG_INFO("node ID set to %u", config.node_id);
      cfg_changed = true;
    }
    break;

#if !BASESTATION
  case CMD_SX1262_SET_TX_POWER:
  {
    int16_t tx_pwr = cmd->arg16[0];   // must be 16-bit signed
    if ((tx_pwr >= RADIO_MIN_POWER) && (tx_pwr <= RADIO_MAX_POWER)) {
      gloria_set_tx_power(tx_pwr);
      send_bs_tx_pwr = tx_pwr;
      LOG_INFO("TX power changed to %ddBm", tx_pwr);
    }
    // make sure the command has an effect if the current radio config is fallback mode 2 (which has a fixed TX power)
    if (send_bs_fail_cnt > SEND_BS_FALLBACK_THRESHOLD) {
      send_bs_fail_cnt = SEND_BS_FALLBACK_THRESHOLD + 1;
    } else if (tx_pwr == CMD_SX1262_FORCE_FALLBACK) {
      // value '100' is used to switch to the fallback modulation
      send_bs_fail_cnt = SEND_BS_FALLBACK_THRESHOLD + 1;
      LOG_INFO("switched to fallback modulation");
    }
  } break;

  case CMD_SX1262_SET_CODETECTION_THRESHOLD:
    if (cmd->arg[0] > 0 && cmd->arg[0] <= 8) {    // must be <= 8 since we need to pack this value into 3 bits in the health_min packet
      codetection_th = cmd->arg[0];
      LOG_INFO("codetection threshold changed to %u", codetection_th);
    }
    break;

  case CMD_SX1262_SET_CAD_MODE:
    send_bs_cad_mode = (cmd->arg[0] != 0);
    LOG_INFO("CAD mode set to %u", send_bs_cad_mode);
    break;

  case CMD_SX1262_SET_HEALTH_MSG_PRIORITY:
    abort_event_on_rtc_trigger = (cmd->arg[0] != 0);
    LOG_INFO("health message priority set to %u", abort_event_on_rtc_trigger);
    break;

  case CMD_SX1262_SET_STAG_WAKEUP_TIMEOUT:
    if ((cmd->arg16[0] >= 100) && (cmd->arg16[0] <= 2000)) {
      stag_wakeup_timeout_ms = cmd->arg16[0];
      LOG_INFO("staggered wakeup timeout set to %ums", stag_wakeup_timeout_ms);
    }
    break;

  case CMD_SX1262_SET_HEALTH_PERIOD:
    if ((cmd->arg16[0] >= NODE_HEALTH_MIN_INTERVAL_S) && cmd->arg16[0] <= NODE_HEALTH_MAX_INTERVAL_S) {
      health_msg_period = ((cmd->arg16[0] + 59) / 60);    // convert to minutes (round up)
      LOG_INFO("health message period set to %lumin", health_msg_period);
    }
    break;

 #if FW_OTA_ENABLE
  case CMD_SX1262_FW_OTA_MODE:
    if (FW_OTA_KEY == 0 || FW_OTA_KEY == cmd->arg16[0]) {
      LOG_INFO("FW update mode set");
      fw_update_mode = true;
      // note: do not enter FW update mode here -> use a low priority task (e.g. debug task)
    } else {
      LOG_WARNING("invalid key for FW update mode");
    }
    break;
 #endif /* FW_OTA_ENABLE */

#endif /* BASESTATION */

#if BASEBOARD
  case CMD_SX1262_BASEBOARD_ENABLE:
  case CMD_SX1262_BASEBOARD_DISABLE:
    sched_cmd.type           = cmd->type;
    sched_cmd.scheduled_time = cmd->arg32[0];
    sched_cmd.arg            = 0;
    if (cmd->arg[4] & 1) {
      /* relative time -> append generation time */
      if (hdr) {
        sched_cmd.scheduled_time += hdr->generation_time / 1000000;
      }
    }
    if (cmd->type == CMD_SX1262_BASEBOARD_ENABLE) {
      sched_cmd.arg = (uint16_t)cmd->arg[6] << 8 | cmd->arg[5];
    }
    if (!list_insert(pending_bb_cmds, sched_cmd.scheduled_time, &sched_cmd)) {
      LOG_WARNING("failed to add command to queue");
      EVENT_WARNING(EVENT_SX1262_QUEUE_FULL, 10);
    } else {
      LOG_VERBOSE("baseboard command %u scheduled (time: %lu)", cmd->type & 0xff, sched_cmd.scheduled_time);
    }
    break;

  case CMD_SX1262_BASEBOARD_ENABLE_PERIODIC:
    config.bb_en.period = (uint32_t)cmd->arg16[1] * 60;  /* convert to seconds */
    if (config.bb_en.period > 0) {
      config.bb_en.starttime = rtc_get_next_timestamp_at_daytime((get_time(0) / 1000000), cmd->arg[0], cmd->arg[1], 0);
      if (config.bb_en.starttime > 0) {
        LOG_INFO("periodic baseboard enable scheduled (next: %u  period: %us)", config.bb_en.starttime, config.bb_en.period);
      } else {
        LOG_WARNING("invalid parameters for periodic enable cmd");
      }
    } else {
      config.bb_en.starttime = 0;
      LOG_INFO("periodic baseboard enable cleared");
    }
    cfg_changed = true;
    break;

  case CMD_SX1262_BASEBOARD_POWER_EXT3:
    if (cmd->arg[0]) {
      PIN_SET(BASEBOARD_EXT3_SWITCH);
      LOG_INFO("EXT3 power enabled");
    } else {
      PIN_CLR(BASEBOARD_EXT3_SWITCH);
      LOG_INFO("EXT3 power disabled");
    }
    break;
#endif /* BASEBOARD */

  default:
    /* unknown command -> command processing failed */
    return false;
    break;
  }

  if (cfg_changed) {
#if NVCFG_ENABLE
    if (nvcfg_save(&config)) {
      LOG_INFO("config saved to NV memory");
    } else {
      LOG_ERROR("failed to save config");
    }
#endif /* NVCFG_ENABLE */
  }

  return true;
}


/* process firmware message on the master; returns true if the message should be forwarded */
bool process_fw_message(dpp_fw_t* fw_msg, uint16_t sender_id, uint16_t target_id)
{
  bool forward = false;

#if FW_OTA_MASTER

  if (fw_msg->component_id == DPP_COMPONENT_ID_SX1262) {

    switch (fw_msg->type) {

    case DPP_FW_TYPE_DATA:
      if (sender_id == config.node_id) {    // only accept msg if it came via BOLT (i.e. sender device ID must match own ID)
        fw_ota_add_data(fw_msg);            // store the data block in flash memory
      }
      break;

    case DPP_FW_TYPE_CHECK:
      if (fw_ota_verify(fw_msg->version, fw_msg->info.len, fw_msg->info.crc)) {
        LOG_INFO("firmware in flash memory verified (FW version: %u  length: %luB  crc: 0x%x)", fw_msg->version, fw_msg->info.len, fw_msg->info.crc);
        msg_buffer.firmware.component_id = DPP_COMPONENT_ID_SX1262;
        msg_buffer.firmware.type         = DPP_FW_TYPE_READY;
        msg_buffer.firmware.version      = fw_msg->version;
        enqueue_message(sender_id, DPP_MSG_TYPE_FW, (uint8_t*)&(msg_buffer.firmware), DPP_FW_HDR_LEN, INTERFACE_BOLT);
      } else {
        LOG_WARNING("FW data validation failed (CRC: 0x%x  length: %luB)", fw_msg->info.crc, fw_msg->info.len);
      }
      break;

    case DPP_FW_TYPE_UPDATE:
      /* initiate the update process by sending a FW CHECK message */
      if ((sender_id == config.node_id) && (target_id != 0) && (target_id != DPP_DEVICE_ID_BROADCAST)) {
        if (fw_ota_get_info(&(msg_buffer.firmware))) {
          enqueue_message(target_id, DPP_MSG_TYPE_FW, 0, DPP_FW_INFO_LEN, INTERFACE_LORA);
          xTaskNotify(xTaskHandle_basestation, BS_TASK_NOTIFY_TX, eSetValueWithOverwrite);
        } else {
          LOG_WARNING("FW not ready for update");
        }
      } else {
        LOG_INFO("ignored FW packet of type %u (unspecified target or invalid sender ID)", DPP_FW_TYPE_UPDATE);
      }
      break;

    case DPP_FW_TYPE_READY:
      LOG_INFO("node %u is ready to update its firmware", sender_id);
      /* trigger FW update on source node */
      msg_buffer.firmware.component_id = DPP_COMPONENT_ID_SX1262;
      msg_buffer.firmware.type         = DPP_FW_TYPE_UPDATE;
      msg_buffer.firmware.version      = fw_msg->version;
      enqueue_message(sender_id, DPP_MSG_TYPE_FW, (uint8_t*)&(msg_buffer.firmware), DPP_FW_HDR_LEN, INTERFACE_LORA);
      xTaskNotify(xTaskHandle_basestation, BS_TASK_NOTIFY_TX, eSetValueWithOverwrite);
      forward = true;   // forward message to BOLT
      break;

    case DPP_FW_TYPE_DATAREQ:
      LOG_INFO("node %u requested %u firmware blocks", sender_id, fw_msg->req.num);
      /* send FW data as min-type messages */
      dpp_message_min_t* msg_min = (dpp_message_min_t*)&msg_buffer;
      uint32_t queue_spaces      = uxQueueSpacesAvailable(xQueueHandle_lora_tx);
      if (queue_spaces > 0) {
        uint16_t num = MIN(fw_msg->req.num, queue_spaces - 1);   // leave one space for the FW CHECK message below
        for (uint16_t i = 0; i < num; i++) {
          if (!fw_ota_get_data(fw_msg->req.block_ids[i], &(msg_min->firmware)) ||
              !enqueue_message(sender_id, DPP_MSG_TYPE_FW_MIN, 0, DPP_FW_DATA_LEN, INTERFACE_LORA)) {
            LOG_WARNING("failed to prepare FW data block %u for transmission", fw_msg->req.block_ids[i]);
            break;
          }
        }
        if (fw_ota_get_info(&(msg_buffer.firmware))) {
          enqueue_message(sender_id, DPP_MSG_TYPE_FW, 0, DPP_FW_INFO_LEN, INTERFACE_LORA);
          xTaskNotify(xTaskHandle_basestation, BS_TASK_NOTIFY_TX, eSetValueWithOverwrite);
        } else {
          LOG_WARNING("firmware not yet verified");
        }
      } else {
        LOG_WARNING("not enough space left in the queue");
      }
      break;

    default:
      LOG_WARNING("unknown FW packet type %u", fw_msg->type);
      break;
    }
  }
  /* else: component ID does not match -> ignore message */

#else  /* FW_OTA_MASTER */

  /* ignore all messages of this type */
  LOG_VERBOSE("FW message ignored");

#endif /* FW_OTA_MASTER */

  return forward;   /* do not forward */
}


#if BASESTATION

/* process a DPP message on the basestation */
void process_message_basestation(dpp_message_t* msg, bool rcvd_from_bolt)
{
  bool forward = false;

  switch (msg->header.type) {

  case DPP_MSG_TYPE_CMD:
  {
    if (msg->header.target_id == config.node_id) {
      if ((msg->cmd.type >> 8) == DPP_COMPONENT_ID_SX1262) {
        LOG_VERBOSE("command received");
        uint32_t evt_val = ((uint32_t)msg->cmd.arg16[0] << 16) | msg->cmd.type;
        /* command successfully executed? */
        if (process_command(&msg->cmd, &msg->header)) {
          LOG_INFO("cmd %u processed", msg->cmd.type);
          EVENT_INFO(EVENT_SX1262_CMD_EXECUTED, evt_val);
        } else {
          LOG_WARNING("failed to process cmd %u", msg->cmd.type);
          EVENT_WARNING(EVENT_SX1262_INV_CMD, evt_val);
        }
      } else {
        /* command is targeted at another DPP component with the same device ID */
        forward = true;
      }
    } else {
      /* target is a not this device; only non-sniffer nodes forward commands */
  #if !BASESTATION_SNIFFER
      if (rcvd_from_bolt) {
        if ((msg->header.target_id >= BS_MIN_NODE_ID) && (msg->header.target_id <= BS_MAX_NODE_ID)) {
          pending_cmds[msg->header.target_id - BS_MIN_NODE_ID] = ((uint32_t)msg->cmd.arg16[0] << 16) | msg->cmd.type;
          LOG_INFO("command %u with arg %u added (will be forwarded to target node %u)", msg->cmd.type, msg->cmd.arg16[0], msg->header.target_id);

        } else if (msg->header.target_id == DPP_DEVICE_ID_BROADCAST) {
          /* do not forward broadcast messages of certain types */
          if (msg->cmd.type != CMD_SX1262_SET_NODE_ID) {
            /* add command for every target node that does not already have a pending command */
            for (uint16_t i = 0; i < (BS_MAX_NODE_ID - BS_MIN_NODE_ID + 1); i++) {
              if (pending_cmds[i] == 0) {
                pending_cmds[i] = ((uint32_t)msg->cmd.arg16[0] << 16) | msg->cmd.type;
              } else {
                LOG_WARNING("broadcast command %u will not be forwarded to node %u (has command %u with arg %u in queue)", msg->cmd.type, BS_MIN_NODE_ID + i, pending_cmds[i] & 0xffff, pending_cmds[i] >> 16);
              }
            }
            LOG_INFO("command %u with arg %u added (will be forwarded to all nodes)", msg->cmd.type, msg->cmd.arg16[0]);
          } else {
            LOG_WARNING("ignoring broadcast command %u", msg->cmd.type);
          }
        } else if ((msg->cmd.type >> 8) == DPP_COMPONENT_ID_SX1262) {
          /* command for some other node with same component ID in the network -> schedule for transmission */
          if (xQueueSend(xQueueHandle_lora_tx, msg, 0)) {
            LOG_INFO("command %u added to TX queue", msg->cmd.type);
            xTaskNotify(xTaskHandle_basestation, BS_TASK_NOTIFY_TX, eSetValueWithOverwrite);
          } else {
            LOG_WARNING("failed to add command %u to TX queue", msg->cmd.type);
          }
        }
      }
      /* else: command received from network, ignore */
  #endif /* BASESTATION_SNIFFER */
    }
  } break;

#if BOLT_ENABLE
  case DPP_MSG_TYPE_TIMESYNC:
  {
    static bool node_info_sent = false;
    LOG_VERBOSE("timestamp %llu received", msg->timestamp);
    set_time(msg->timestamp, 0, true);
    if (!node_info_sent) {
      generate_node_info();
      node_info_sent = true;
    }
  } break;
#endif /* BOLT_ENABLE */

#if FW_OTA_ENABLE
  case DPP_MSG_TYPE_FW:
  case DPP_MSG_TYPE_FW_MIN:
  {
    dpp_fw_t* fw_msg = &(msg->firmware);
    if (msg->header.type & DPP_MSG_TYPE_MIN) {
      fw_msg = &(((dpp_message_min_t*)msg)->firmware);
    }
    if (process_fw_message(fw_msg, msg->header.device_id, (msg->header.type == DPP_MSG_TYPE_FW_MIN) ? 0 : msg->header.target_id)) {
      forward = true;
    }
  } break;
#endif /* FW_OTA_ENABLE */

  case DPP_MSG_TYPE_GEO_ACQ_AGGR:
    /* print message content */
    LOG_INFO("aggregated geophone acquisition msg with %u blocks", msg->data_aggr.block_cnt);
    for (uint16_t i = 0; i < msg->data_aggr.block_cnt; i++) {
      LOG_INFO("node %u:  %5u  %5u  %5u  %5u  %5u", msg->data_aggr.geo_acq_blocks[i].node_id,
                                                    msg->data_aggr.geo_acq_blocks[i].geo_acq_min.event_id,
                                                    msg->data_aggr.geo_acq_blocks[i].geo_acq_min.start_time,
                                                    msg->data_aggr.geo_acq_blocks[i].geo_acq_min.amplitude,
                                                    msg->data_aggr.geo_acq_blocks[i].geo_acq_min.duration,
                                                    msg->data_aggr.geo_acq_blocks[i].geo_acq_min.trg_count);
    }
    forward = true;
    break;

  case DPP_MSG_TYPE_HEALTH_MIN:
    LOG_INFO("node %u health:  %u %u %d %u %u %u %u %u %u %u %u %u %d 0x%x 0x%x", msg->header.device_id,
                                                                                  msg->health_min.uptime,
                                                                                  msg->health_min.supply_vcc,
                                                                                  msg->health_min.temperature,
                                                                                  msg->health_min.humidity,
                                                                                  msg->health_min.nv_mem,
                                                                                  msg->health_min.stack_wm,
                                                                                  msg->health_min.cpu_dc_app,
                                                                                  msg->health_min.cpu_dc_com,
                                                                                  msg->health_min.radio_rx_dc,
                                                                                  msg->health_min.radio_tx_dc,
                                                                                  msg->health_min.radio_hop_cnt,
                                                                                  msg->health_min.radio_prr,
                                                                                  msg->health_min.radio_rssi,
                                                                                  msg->health_min.events,
                                                                                  msg->health_min.config);
    forward = true;
    break;

  case DPP_MSG_TYPE_HEALTH_AGGR:
    /* print message content */
    LOG_INFO("aggregated health msg with %u blocks", msg->data_aggr.block_cnt);
    for (uint16_t i = 0; i < msg->data_aggr.block_cnt; i++) {
      LOG_INFO("\tnode %u:\t%u %u %d %u %u %u %u %u %u %u %u %u %d", msg->data_aggr.health_blocks[i].node_id,
                                                                     msg->data_aggr.health_blocks[i].health_min.uptime,
                                                                     msg->data_aggr.health_blocks[i].health_min.supply_vcc,
                                                                     msg->data_aggr.health_blocks[i].health_min.temperature,
                                                                     msg->data_aggr.health_blocks[i].health_min.humidity,
                                                                     msg->data_aggr.health_blocks[i].health_min.nv_mem,
                                                                     msg->data_aggr.health_blocks[i].health_min.stack_wm,
                                                                     msg->data_aggr.health_blocks[i].health_min.cpu_dc_app,
                                                                     msg->data_aggr.health_blocks[i].health_min.cpu_dc_com,
                                                                     msg->data_aggr.health_blocks[i].health_min.radio_rx_dc,
                                                                     msg->data_aggr.health_blocks[i].health_min.radio_tx_dc,
                                                                     msg->data_aggr.health_blocks[i].health_min.radio_hop_cnt,
                                                                     msg->data_aggr.health_blocks[i].health_min.radio_prr,
                                                                     msg->data_aggr.health_blocks[i].health_min.radio_rssi);
    }
    forward = true;
    break;

  case DPP_MSG_TYPE_NODE_INFO:
  case DPP_MSG_TYPE_EVENT_MIN:
    forward = true;
    break;

  case DPP_MSG_TYPE_ACK_COMMAND:
    /* ignore */
    break;

  default:
    LOG_VERBOSE("message of unknown type %u received from ID %u", msg->header.type, msg->header.device_id);
    EVENT_WARNING(EVENT_SX1262_MSG_IGNORED, ((uint32_t)msg->header.type << 16) | msg->header.device_id);
    /* do not forward packets of unknown types */
    break;
  }

  /* message forwarding to BOLT */
#if BOLT_ENABLE
  if (!rcvd_from_bolt && forward) {
    /* if target ID was 0 (sink), then replace it with the node ID */
    if (!(msg->header.type & DPP_MSG_TYPE_MIN) && (msg->header.target_id == DPP_DEVICE_ID_SINK)) {
      msg->header.target_id = config.node_id;
      ps_update_msg_crc(msg);
    }
    /* forward to BOLT */
    uint16_t msg_len = (msg->header.type & DPP_MSG_TYPE_MIN) ? (DPP_MSG_MIN_LEN(msg)) : (DPP_MSG_LEN(msg));
    if (bolt_write((uint8_t*)msg, msg_len)) {
      LOG_VERBOSE("msg written to BOLT");
    } else {
      LOG_WARNING("msg dropped (BOLT queue full)");
      EVENT_WARNING(EVENT_SX1262_BOLT_ERROR, 1);
    }
  }
#else /* BOLT_ENABLE */
  (void)forward;          // to get rid of compiler warning if BOLT_ENABLE not set
#endif /* BOLT_ENABLE */
}

#else /* BASESTATION */


/* process a DPP message on a source node */
void process_message_sourcenode(dpp_message_t* msg)
{
  switch (msg->header.type) {

  case DPP_MSG_TYPE_ACK_COMMAND:
    if ((msg->header.target_id == config.node_id) || (msg->header.target_id == DPP_DEVICE_ID_BROADCAST)) {
      /* extract the commands */
      for (uint16_t i = 0; i < msg->ack_cmd.num_cmds; i++) {
        if (msg->ack_cmd.commands[i].target_id == config.node_id) {
          dpp_command_t* cmd = (dpp_command_t*)&msg->ack_cmd.commands[i].cmd;
          /* different component ID? -> send to BOLT */
          if ((cmd->type >> 8) != DPP_COMPONENT_ID_SX1262) {
            generate_command(cmd->type, cmd->arg32, msg->header.payload_len - 2); // payload: | type (2 bytes) | argument |
          } else {
            uint32_t evt_val = ((uint32_t)msg->ack_cmd.commands[i].cmd.arg << 16) | msg->ack_cmd.commands[i].cmd.type;
            if (process_command(cmd, 0)) {
              LOG_INFO("cmd %u processed", msg->ack_cmd.commands[i].cmd.type);
              EVENT_INFO(EVENT_SX1262_CMD_EXECUTED, evt_val);
            } else {
              LOG_WARNING("failed to process cmd %u", msg->ack_cmd.commands[i].cmd.type);
              EVENT_WARNING(EVENT_SX1262_INV_CMD, evt_val);
            }
          }
        }
      }
    } else {
      LOG_VERBOSE("message of type %u ignored (target ID mismatch)");
      EVENT_WARNING(EVENT_SX1262_MSG_IGNORED, ((uint32_t)msg->header.type << 16) | msg->header.device_id);
    }
    break;

  case DPP_MSG_TYPE_GEOPHONE_ACQ:
    /* write into FSK TX queue */
    if (xQueueSend(xQueueHandle_fsk_tx, msg, 0)) {
      LOG_VERBOSE("msg added to FSK TX queue");
    } else {
      LOG_WARNING("failed to add message of type %u to FSK TX queue", DPP_MSG_TYPE_GEOPHONE_ACQ);
      EVENT_WARNING(EVENT_SX1262_QUEUE_FULL, 2);
    }
    break;

  case DPP_MSG_TYPE_APP_HEALTH:
    /* keep a copy of the message */
    if (msg->header.device_id == config.node_id) { /* check ID */
      memcpy(&last_app_health, &msg->app_health, DPP_APP_HEALTH_LEN);
      app_health_rcvd = true;
    }
    break;

  default:
    LOG_VERBOSE("message of unknown type %u received from ID %u", msg->header.type, msg->header.device_id);
    EVENT_WARNING(EVENT_SX1262_MSG_IGNORED, ((uint32_t)msg->header.type << 16) | msg->header.device_id);
    break;
  }
}

#endif /* BASESTATION */


/* processes incoming messages (BOLT or packet from base station via LoRa)
 * returns true if message is valid */
bool process_message(dpp_message_t* msg, bool rcvd_from_bolt)
{
  /* check message type, length and CRC */
  if (!ps_validate_msg(msg)) {
    LOG_ERROR("msg with invalid length or CRC (src: %u  payload: %uB  type: %u)", msg->header.device_id, msg->header.payload_len, msg->header.type);
    EVENT_WARNING(EVENT_SX1262_INV_MSG, ((uint32_t)msg->header.payload_len) << 16 | msg->header.device_id);
    return false;
  }
  /* message of type MIN (reduced header) are always targeted at the sink */
  uint16_t target_id = (msg->header.type & DPP_MSG_TYPE_MIN) ? DPP_DEVICE_ID_SINK : msg->header.target_id;
  LOG_VERBOSE("msg type: %u  src: %u  payload len: %uB  target: %u", msg->header.type, msg->header.device_id, msg->header.payload_len, target_id);

#if BASESTATION
  process_message_basestation(msg, rcvd_from_bolt);
#else /* BASESTATION */
  process_message_sourcenode(msg);
#endif /* BASESTATION */

  return true;
}


#if BASEBOARD

void process_scheduled_bb_commands(void)
{
  uint32_t curr_time = rtc_get_unix_timestamp();
  const scheduled_cmd_t* next_cmd = list_get_head(pending_bb_cmds);
  if (next_cmd) {
    /* there are pending commands */
    /* anything that needs to be executed now? */
    while (next_cmd && next_cmd->scheduled_time <= curr_time) {
      switch (next_cmd->type) {
      case CMD_SX1262_BASEBOARD_ENABLE:
        BASEBOARD_ENABLE();
        LOG_INFO("baseboard enabled");
        generate_command(CMD_BASEBOARD_WAKEUP_MODE, (uint32_t*)&next_cmd->arg, 2);
        break;
      case CMD_SX1262_BASEBOARD_DISABLE:
        BASEBOARD_DISABLE();
        LOG_INFO("baseboard disabled");
        bolt_get_write_cnt(true);         // assume all messages have been read from bolt at this point -> reset counter
        break;
      default:
        break;
      }
      list_remove_head(pending_bb_cmds, 0);
      next_cmd = list_get_head(pending_bb_cmds);
    }
  }
  /* check the periodic baseboard enable */
  if (config.bb_en.starttime > 0 && config.bb_en.starttime <= curr_time) {
    BASEBOARD_ENABLE();
    while (config.bb_en.period > 0 && config.bb_en.starttime < curr_time) {
      config.bb_en.starttime += config.bb_en.period;
    }
    LOG_INFO("baseboard enabled (next wakeup in %lus)", config.bb_en.starttime - curr_time);
  }
}

#endif /* BASEBOARD */


/*
 * calling this function from an interrupt context can lead to erratic behaviour
 * if data is 0, the data in the static msg_buffer will be used
 * NOTE: do not generate EVENTS in this function until the message has been written to the target interface / queue
 */
bool enqueue_message(uint16_t recipient,
                     dpp_message_type_t type,
                     const uint8_t* data,
                     uint8_t len,
                     interface_t target)
{
  /* separate sequence number for each interface */
  static uint16_t seq_no_bolt = 0;
  static uint16_t seq_no_fsk  = 0;
  static uint16_t seq_no_lora = 0;

  /* check if in interrupt context */
  if (IS_INTERRUPT()) {
    LOG_WARNING("cannot enqueue messages from ISR (msg of type %u dropped)", type);
    return false;
  }

  /* compose the message */
  uint8_t msg_buffer_len = ps_compose_msg(recipient, type, data, len, &msg_buffer);
  if (!msg_buffer_len) {
    return false;
  }

  /* adjust the sequence number (per interface) */
  if ((type & DPP_MSG_TYPE_MIN) == 0) {
    if (target == INTERFACE_BOLT) {
      msg_buffer.header.seqnr = seq_no_bolt++;
    } else if (target == INTERFACE_FSK) {
      msg_buffer.header.seqnr = seq_no_fsk++;
    } else if (target == INTERFACE_LORA) {
      msg_buffer.header.seqnr = seq_no_lora++;
    }
    ps_update_msg_crc(&msg_buffer);
  }

  /* copy the message into the appropriate queue */
  switch (target) {

#if BOLT_ENABLE
  case INTERFACE_BOLT:
    if (bolt_write((uint8_t*)&msg_buffer, msg_buffer_len)) {
      LOG_VERBOSE("msg of type %u and length %u bytes written to BOLT", type, msg_buffer_len);
      return true;
    }
    LOG_WARNING("msg of type %u and length %u bytes dropped (BOLT queue full)", type, msg_buffer_len);
    EVENT_WARNING(EVENT_SX1262_BOLT_ERROR, 1);
    break;
#endif /* BOLT_ENABLE */

  case INTERFACE_FSK:
    if (xQueueSend(xQueueHandle_fsk_tx, &msg_buffer, 0)) {
      LOG_VERBOSE("msg of type %u and length %u bytes added to FSK TX queue", type, msg_buffer_len);
      return true;
    }
    LOG_WARNING("msg of type %u dropped (FSK TX queue full)", type);
    EVENT_WARNING(EVENT_SX1262_QUEUE_FULL, 2);
    break;

  case INTERFACE_LORA:
    if (xQueueSend(xQueueHandle_lora_tx, &msg_buffer, 0)) {
      LOG_VERBOSE("msg of type %u and length %u bytes added to LoRa TX queue", type, msg_buffer_len);
      return true;
    }
    LOG_WARNING("msg of type %u dropped (LoRa TX queue full)", type);
    EVENT_WARNING(EVENT_SX1262_QUEUE_FULL, 4);
    break;

  default:
    LOG_WARNING("invalid target interface");
    break;
  }
  return false;
}


/* send the timestamp to the APP processor */
void generate_timestamp(uint64_t trq_timestamp)
{
  msg_buffer.timestamp = get_time(trq_timestamp);
#if BOLT_ENABLE
  enqueue_message(config.node_id, DPP_MSG_TYPE_TIMESYNC, 0, 0, INTERFACE_BOLT);
#endif /* BOLT_ENABLE */
  LOG_INFO("requested timestamp sent (%llu)", msg_buffer.timestamp);
}


/* generate an aggregated AE event message
 * reads and processes all messages from the input queue and writes the aggregated message into the output queue */
void generate_aggr_message(dpp_message_type_t msg_type)
{
  static dpp_data_aggr_t data_aggr;
  dpp_message_type_t     aggr_type;

  LOG_INFO("number of messages in the FSK RX queue: %i", uxQueueMessagesWaiting(xQueueHandle_fsk_rx));
  data_aggr.block_cnt  = 0;
  if (msg_type == DPP_MSG_TYPE_GEOPHONE_ACQ) {
    data_aggr.block_size = DPP_DATA_AGGR_GEO_ACQ_MIN_LEN;
    aggr_type = DPP_MSG_TYPE_GEO_ACQ_AGGR;
  } else if (msg_type == DPP_MSG_TYPE_HEALTH_MIN) {
    data_aggr.block_size = DPP_DATA_AGGR_HEALTH_MIN_LEN;
    aggr_type = DPP_MSG_TYPE_HEALTH_AGGR;
  } else {
    LOG_WARNING("unknown aggregation message type %u", msg_type);
    return;
  }

  while (xQueueReceive(xQueueHandle_fsk_rx, (uint8_t*)&msg_buffer, 0)) {
    if (msg_buffer.header.type != msg_type) {
      LOG_INFO("message of type %u ignored", msg_buffer.header.type);
      continue;
    }
    if (msg_buffer.header.type == DPP_MSG_TYPE_GEOPHONE_ACQ) {
      data_aggr.geo_acq_blocks[data_aggr.block_cnt].node_id = msg_buffer.header.device_id;
      data_aggr.geo_acq_blocks[data_aggr.block_cnt].geo_acq_min.duration   = msg_buffer.geo_acq.samples * (1 << msg_buffer.geo_acq.adc_sps);
      data_aggr.geo_acq_blocks[data_aggr.block_cnt].geo_acq_min.event_id   = msg_buffer.geo_acq.acq_id;
      /* geophone returns 24-bit raw ADC values for peak+ and peak- (we assume offset binary format here with a midpoint at 0x800000!)
       * -> convert to a 16-bit value and calculate the amplitude */
      uint16_t ampl_pos = ABS((int32_t)(msg_buffer.geo_acq.peak_pos_val >> 7) -  (1L << 16));
      uint16_t ampl_neg = ABS((int32_t)(msg_buffer.geo_acq.peak_neg_val >> 7) - ((1L << 16) - 1));
      data_aggr.geo_acq_blocks[data_aggr.block_cnt].geo_acq_min.amplitude  = MAX(ampl_pos, ampl_neg);
      data_aggr.geo_acq_blocks[data_aggr.block_cnt].geo_acq_min.start_time = msg_buffer.geo_acq.start_time;   // start time has already been converted to a 16-bit offset value in ms
      data_aggr.geo_acq_blocks[data_aggr.block_cnt].geo_acq_min.trg_count  = msg_buffer.geo_acq.trg_count_pos + msg_buffer.geo_acq.trg_count_neg;
      data_aggr.block_cnt++;
      if (data_aggr.block_cnt >= (DPP_MSG_PAYLOAD_LEN - DPP_DATA_AGGR_HDR_LEN) / DPP_DATA_AGGR_GEO_ACQ_MIN_LEN) {
        break;    // can't fit any more data into this message
      }

    } else if (msg_buffer.header.type == DPP_MSG_TYPE_HEALTH_MIN) {
      data_aggr.health_blocks[data_aggr.block_cnt].node_id = msg_buffer.header.device_id;
      memcpy(&data_aggr.health_blocks[data_aggr.block_cnt].health_min, &msg_buffer.health_min, DPP_HEALTH_MIN_LEN);
      data_aggr.block_cnt++;
      if (data_aggr.block_cnt >= (DPP_MSG_PAYLOAD_LEN - DPP_DATA_AGGR_HDR_LEN) / DPP_DATA_AGGR_HEALTH_MIN_LEN) {
        break;    // can't fit any more data into this message
      }

    } else {
      LOG_INFO("can't process message of type %u", msg_buffer.header.type);
    }
  }
  /* still data in the queue? */
  if (uxQueueMessagesWaiting(xQueueHandle_fsk_rx)) {
    LOG_WARNING("there are still messages in the FSK RX queue (not all data fits into aggregated msg)");
  }
  /* any data aggregated? */
  if (data_aggr.block_cnt) {
    LOG_INFO("%u messages of type %u aggregated", data_aggr.block_cnt, msg_type);
    enqueue_message(DPP_DEVICE_ID_SINK, aggr_type, (uint8_t*)&data_aggr, 0, INTERFACE_LORA);
#if BOLT_ENABLE
    enqueue_message(config.node_id, aggr_type, (uint8_t*)&data_aggr, 0, INTERFACE_BOLT);    // also send to BOLT (APP) for logging
#endif /* BOLT_ENABLE */
  }
}


void generate_node_info(void)
{
  /* fill in the 'config' field */
  uint32_t cfg_field = 0;
#if BASESTATION
  if (BASESTATION_FALLBACK) {
    cfg_field |= (1 << 0);
  }
  if (BASESTATION_SNIFFER) {
    cfg_field |= (1 << 1);
  }
#endif /* BASESTATION */

  ps_compose_nodeinfo(&msg_buffer, config.rst_cnt, cfg_field);

  LOG_INFO("node info msg generated");
  enqueue_message(DPP_DEVICE_ID_SINK, DPP_MSG_TYPE_NODE_INFO, 0, 0, (BASESTATION ? INTERFACE_BOLT : INTERFACE_LORA));

  EVENT_INFO(EVENT_SX1262_RESET, (((uint32_t)config.rst_cnt) << 16) | msg_buffer.node_info.rst_flag);
}


/* generate a minimal node health message (combined APP and COM health) */
void generate_node_health_min(interface_t target)
{
  memset(&msg_buffer.health_min, 0, sizeof(dpp_health_min_t));

  /* fill in the COM processor specific values */
  msg_buffer.health_min.cpu_dc_com  = rtos_get_cpu_dc();
  msg_buffer.health_min.stack_wm    = rtos_check_stack_usage();
  msg_buffer.health_min.uptime      = lptimer_now() / (LPTIMER_SECOND * 3600);
  msg_buffer.health_min.radio_rx_dc = radio_get_rx_dc() / 100;         // convert from ppm to [% * 10^2]
  msg_buffer.health_min.radio_tx_dc = radio_get_tx_dc() / 100;
  msg_buffer.health_min.radio_prr   = radio_get_prr(true) / 100;       // retrieve and reset counter
  get_radio_stats(&msg_buffer.health_min.radio_rssi, 0, &msg_buffer.health_min.radio_hop_cnt, true);    // retrieve and reset stats
  msg_buffer.health_min.events      = detected_events;
  msg_buffer.health_min.config      = ((send_bs_tx_pwr + 9)            & 0x1f)       |   // 5 bits for TX power (0..31 -> -9..22)
                                      (((stag_wakeup_timeout_ms / 100) & 0x1f) << 5) |   // 5 bits for staggered wakeup timeout (0..31 -> (0..31) * 100ms)
                                      (((codetection_th - 1)           & 0x7) << 10) |   // 3 bits for codetection threshold (0..7 -> 1..8)
                                      ((send_bs_cad_mode               & 0x1) << 13) |   // 1 bit for CAD on/off
                                      ((abort_event_on_rtc_trigger     & 0x1) << 14) |   // 1 bit for health message priority
                                      (((send_bs_fail_cnt > SEND_BS_FALLBACK_THRESHOLD) & 0x1) << 15);  // 1 bit for the modulation (bit set = fallback)

  /* reset counters */
  rtos_reset_cpu_dc();
  radio_dc_counter_reset();
  detected_events = 0;

  if (app_health_rcvd) {
    /* use the content of the last received app health message */
    msg_buffer.health_min.cpu_dc_app  = last_app_health.cpu_dc;
    msg_buffer.health_min.nv_mem      = last_app_health.nv_mem;
    if (last_app_health.stack > msg_buffer.health_min.stack_wm) {
      msg_buffer.health_min.stack_wm  = (last_app_health.stack | 0x80);           // set MSB to indicate that it was the APP processor
    }
    msg_buffer.health_min.supply_vcc  = (last_app_health.supply_vcc + 50) / 100;  // convert from milliVolt to deciVolt
    if (msg_buffer.health_min.uptime > (last_app_health.uptime / 3600)) {         // keep the smaller uptime
      msg_buffer.health_min.uptime    = last_app_health.uptime / 3600;            // convert from seconds to hours
    }
    msg_buffer.health_min.temperature = last_app_health.temperature / 100;        // convert to °C
    msg_buffer.health_min.humidity    = last_app_health.humidity / 100;           // convert to percentage
  }

  LOG_INFO("health message generated");
  enqueue_message(DPP_DEVICE_ID_SINK, DPP_MSG_TYPE_HEALTH_MIN, 0, 0, target);
#if BOLT_ENABLE
  enqueue_message(config.node_id, DPP_MSG_TYPE_HEALTH_MIN, 0, 0, INTERFACE_BOLT);    // also send to BOLT (APP) for logging
#endif /* BOLT_ENABLE */

  app_health_rcvd = false;    // mark the current app health message as invalid / used
}


/* generates an artificial AE event (geophone_acq) message */
void generate_geo_acq_msg(void)
{
  static uint32_t acq_id = 0;

  msg_buffer.geo_acq.samples       = 2000;
  msg_buffer.geo_acq.adc_sps       = 0;
  msg_buffer.geo_acq.acq_id        = acq_id++;
  msg_buffer.geo_acq.peak_pos_val  = 0;
  msg_buffer.geo_acq.peak_neg_val  = 0;
  msg_buffer.geo_acq.start_time    = 0;
  msg_buffer.geo_acq.trg_count_pos = 1;
  msg_buffer.geo_acq.trg_count_neg = 1;

  enqueue_message(DPP_DEVICE_ID_SINK, DPP_MSG_TYPE_GEOPHONE_ACQ, 0, 0, INTERFACE_FSK);
  LOG_INFO("synthetic geophone acq message generated");
}


/* returns true if the aggregated message is of interest */
bool analyze_aggr_message(const dpp_message_t* msg)
{
  if (!msg) {
    return false;
  }
  if (msg->header.type == DPP_MSG_TYPE_HEALTH_AGGR) {
    return true;    /* keep */
  }
  if ((msg->header.type == DPP_MSG_TYPE_GEO_ACQ_AGGR) && (msg->data_aggr.block_cnt >= codetection_th)) {
    /* go through all aggregated events */
    uint32_t i;
    for (i = 0; i < msg->data_aggr.block_cnt; i++) {
      //TODO calculate some metric
    }
    //TODO decide whether data is of interest
    return true;    /* data is of interest */
  }
  /* event is not of interest */
  return false;
}


void mark_event(dpp_event_type_t type)
{
  if ((type >> 8) == DPP_COMPONENT_ID_SX1262) {
    switch (type) {
    case EVENT_SX1262_CMD_EXECUTED:
      detected_events |= 0x1;
      break;
    case EVENT_SX1262_QUEUE_FULL:
      detected_events |= 0x2;
      break;
    case EVENT_SX1262_INV_MSG:
      detected_events |= 0x4;
      break;
    case EVENT_SX1262_INV_CMD:
      detected_events |= 0x8;
      break;
    case EVENT_SX1262_MSG_IGNORED:
      detected_events |= 0x10;
      break;
    case EVENT_SX1262_BOLT_ERROR:
      detected_events |= 0x20;
      break;
    case EVENT_SX1262_STACK_WM:
      detected_events |= 0x40;
      break;
    case EVENT_SX1262_HOST_ID_ERROR:
      detected_events |= 0x80;
      break;
    case EVENT_SX1262_SCHED_MISSED:
      detected_events |= 0x100;
      break;
    case EVENT_SX1262_MUTEX_ERROR:
      detected_events |= 0x200;
      break;
    case EVENT_SX1262_TSYNC_DRIFT:
      detected_events |= 0x400;
      break;
    case EVENT_SX1262_RTC_ERROR:
      detected_events |= 0x800;
      break;
    case EVENT_SX1262_TIMEOUT:
      detected_events |= 0x1000;
      break;
    case EVENT_SX1262_TX_FAILED:
      detected_events |= 0x2000;
      break;
    case EVENT_SX1262_TIME_UPDATED:
      detected_events |= 0x4000;
      break;
    /*case EVENT_SX1262_:
      detected_events |= 0x8000;
      break;*/
    default:
      break;
    }
  }
}


void generate_event(event_msg_level_t level, dpp_event_type_t type, uint32_t val)
{
  if (event_msg_level < level) {
    return;
  }
  mark_event(type);
  if (event_msg_target == EVENT_MSG_TARGET_UART) {
    if (level == EVENT_MSG_LEVEL_INFO) {
      LOG_INFO("event 0x%02x occurred (value: 0x%02lx)", type, val);
    } else if (level == EVENT_MSG_LEVEL_WARNING) {
      LOG_WARNING("event 0x%02x occurred (value: 0x%02lx)", type, val);
    } else if (level == EVENT_MSG_LEVEL_ERROR) {
      LOG_ERROR("event 0x%02x occurred (value: 0x%02lx)", type, val);
    } else if (level == EVENT_MSG_LEVEL_VERBOSE) {
      LOG_VERBOSE("event 0x%02x occurred (value: 0x%02lx)", type, val);
    }
  } else {
    dpp_event_t event;
    event.type = type;
    event.value = val;
    if (event_msg_target == EVENT_MSG_TARGET_BOLT) {
#if BOLT_ENABLE
      /* do not report errors about BOLT via BOLT */
      if (type != EVENT_SX1262_BOLT_ERROR) {
        if (!enqueue_message(config.node_id, DPP_MSG_TYPE_EVENT, (uint8_t*)&event, 0, INTERFACE_BOLT)) {    // if sent to BOLT, use own ID as target ID
          LOG_ERROR("failed to enqueue event of type %u", type);
        }
      }
#endif /* BOLT_ENABLE */
    } else if (event_msg_target == EVENT_MSG_TARGET_NETWORK) {
      if (!enqueue_message(DPP_DEVICE_ID_SINK, DPP_MSG_TYPE_EVENT, (uint8_t*)&event, 0, INTERFACE_FSK)) {
        LOG_ERROR("failed to enqueue event of type %u", type);
      }
    } else {
      LOG_WARNING("invalid event target");
    }
  }
}


/* send a command to the app processor */
void generate_command(dpp_command_type_t cmd, const uint32_t* args, uint32_t arg_len)
{
  msg_buffer.cmd.type = cmd;
  if (args && arg_len) {
    memcpy(msg_buffer.cmd.arg32, args, arg_len);
  }
  enqueue_message(config.node_id, DPP_MSG_TYPE_CMD, 0, arg_len + DPP_COMMAND_HDR_LEN, INTERFACE_BOLT);
}


#if BASESTATION

uint32_t get_pending_command(uint16_t node_id)
{
  if (node_id >= BS_MIN_NODE_ID && node_id <= BS_MAX_NODE_ID) {
    uint32_t cmd = pending_cmds[node_id - BS_MIN_NODE_ID];
    pending_cmds[node_id - BS_MIN_NODE_ID] = 0;              // clear
    return cmd;
  }
  return 0;
}

#endif /* BASESTATION */

#if BASEBOARD

bool schedule_bb_command(uint32_t sched_time, dpp_command_type_t cmd_type, uint16_t arg)
{
  scheduled_cmd_t cmd;

  cmd.scheduled_time = sched_time;
  cmd.type           = cmd_type;
  cmd.arg            = arg;

  return list_insert(pending_bb_cmds, sched_time, &cmd);
}

#endif /* BASEBOARD */


void set_event_level(event_msg_level_t level)
{
  event_msg_level = level;
}


void set_event_target(event_msg_target_t target)
{
  event_msg_target = target;
}


