/*
 * Copyright (c) 2020 - 2021, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * STeC base station task
 */

#include "main.h"

#if BASESTATION


/* Macros */

#define NOTIFY_BS_TASK(val)           xTaskNotify(xTaskHandle_basestation, val, eSetValueWithOverwrite);
#define NOTIFY_BS_TASK_FROM_ISR(val)  xTaskNotifyFromISR(xTaskHandle_basestation, val, eSetValueWithOverwrite, NULL)

#define BASESTATION_LORA_MODULATION   ((BASESTATION_FALLBACK) ? (SEND_BS_LORA_FALLBACK_MODULATION) : (SEND_BS_LORA_MODULATION))
#define BASESTATION_LORA_BAND         ((BASESTATION_FALLBACK) ? (SEND_BS_LORA_FALLBACK_BAND) : (SEND_BS_LORA_BAND))

#define BASESTATION_CONTINUOUS_RX     0 //BASESTATION_SNIFFER         // sniffer does not transmit and can thus be in continuous RX mode


/* Variables */

extern TaskHandle_t  xTaskHandle_basestation;
extern TaskHandle_t  xTaskHandle_bolt;
extern TaskHandle_t  xTaskHandle_debug;
extern TaskHandle_t  xTaskHandle_timesync;
extern QueueHandle_t xQueueHandle_lora_rx;
extern QueueHandle_t xQueueHandle_lora_tx;

static uint8_t msg_buffer[RADIO_MAX_PAYLOAD_SIZE];
static bool    transmitting = false;


/* Function prototypes */

void basestation_tx_done(void);
void basestation_rx_done(uint8_t* payload, uint16_t size, int16_t rssi, int8_t snr, bool crc_error);
void basestation_rx_timeout(bool crc_error);
void basestation_prepare_ack(const dpp_message_t* msg);
void basestation_do_cad(void);
void basestation_sample_rssi(void);
void basestation_config_radio(void);


/* Functions */

void task_basestation(void const * argument)
{
  uint64_t last_rx_start = 0;
  bool     prev_was_tx   = false;

  LOG_VERBOSE("base station started (LoRa SF%u @%u.%03uMHz %ddBm)", (uint16_t)radio_modulations[BASESTATION_LORA_MODULATION].datarate, (uint16_t)(radio_bands[BASESTATION_LORA_BAND].centerFrequency / 1000000), (uint16_t)((radio_bands[BASESTATION_LORA_BAND].centerFrequency / 1000) % 1000), BS_LORA_TX_POWER);
#if BASESTATION_SNIFFER
  LOG_VERBOSE("packet sniffer mode");
#endif /* BASESTATION_SNIFFER */
  LOG_VERBOSE("node ID range: %u - %u", BS_MIN_NODE_ID, BS_MAX_NODE_ID);

  // set LoRa TX and RX config
  radio_standby();
  radio_set_config_rxtx(true,
                        BASESTATION_LORA_BAND,
                        radio_modulations[BASESTATION_LORA_MODULATION].datarate,
                        BS_LORA_TX_POWER,
                        radio_modulations[BASESTATION_LORA_MODULATION].bandwidth,
                        radio_modulations[BASESTATION_LORA_MODULATION].preambleLen,
                        radio_modulations[BASESTATION_LORA_MODULATION].coderate,
                        0,
                        false,
                        0,
                        true);
  radio_set_rx_gain(true);
  radio_set_irq_mode(IRQ_MODE_RX_TX_CRC);

  //basestation_do_cad();         // uncomment to run CAD instead of regular base station code
  //basestation_sample_rssi();    // uncomment to continuously sample RSSI values (e.g. to determine the noise floor)

  /* make sure the task runs */
  NOTIFY_BS_TASK(BS_TASK_NOTIFY_RX);

  for (;;)
  {
    /* wait until task gets unblocked */
    uint32_t notification = 0;
    led_off(LED_SYSTEM);
    xTaskNotifyWait(0, ULONG_MAX, &notification, BASESTATION_CONTINUOUS_RX ? pdMS_TO_TICKS(BS_TASK_WAKEUP_PERIOD_MS) : portMAX_DELAY);
    led_on(LED_SYSTEM);

    /* if currently transmitting, then put task back to blocked state */
    if (transmitting) {
      continue;
    }
    if (notification == BS_TASK_NOTIFY_TX) {
      /* process message from RX queue */
      dpp_message_t* msg = (dpp_message_t*)msg_buffer;
      if (xQueueReceive(xQueueHandle_lora_rx, msg_buffer, 0)) {
        if (process_message(msg, false)) {
  #if !BASESTATION_SNIFFER
          /* compose and append ACK to LoRa TX queue */
          basestation_prepare_ack(msg);
  #endif /* BASESTATION_SNIFFER */
  #if FLOCKLAB
          /* toggle debug pin and print out the current time (used to map the local BS time to the true UNIX time) */
          PIN_CLR(FLOCKLAB_INT2);
          PIN_SET(FLOCKLAB_INT2);
          LOG_VERBOSE("base station current time: %llu", get_time(0));
          PIN_CLR(FLOCKLAB_INT2);
  #endif /* FLOCKLAB */
        }
      }
  #if !BASESTATION_SNIFFER      // sniffer should not transmit any messages
      /* if there is a message in the TX queue, transmit it */
      if (xQueueReceive(xQueueHandle_lora_tx, msg_buffer, 0)) {
        /* if previous operating mode was TX, then wait some time to give the receiver nodes time to process the previously transmitted packet */
        if (prev_was_tx) {
          vTaskDelay(pdMS_TO_TICKS(50));
        }
        /* get the packet length */
        uint8_t pkt_len;
        if (msg->header.type & DPP_MSG_TYPE_MIN) {
          pkt_len = DPP_MSG_MIN_LEN(msg);
        } else {
          pkt_len = DPP_MSG_LEN(msg);
          /* update the generation time and packet CRC */
          msg->header.generation_time = get_time(0) + BS_ACK_TIMESTAMP_OFS_US;
          ps_update_msg_crc(msg);
        }
        /* start the packet transmission */
        radio_set_tx_callback(&basestation_tx_done);
        transmitting = true;
        prev_was_tx  = true;
        radio_transmit(msg_buffer, pkt_len);
        LOG_VERBOSE("sending packet of size %uB...", pkt_len);
      } else
  #endif /* BASESTATION_SNIFFER */
      {
        NOTIFY_BS_TASK(BS_TASK_NOTIFY_RX);
      }

    } else if ((!BASESTATION_CONTINUOUS_RX && (notification == BS_TASK_NOTIFY_RX)) || (radio_get_status() != RF_RX_RUNNING)) {
      //LOG_VERBOSE("receive mode");
      /* set the base station into RX mode to listen for messages from the sensor nodes */
      radio_set_rx_callback(&basestation_rx_done);
      radio_set_timeout_callback(&basestation_rx_timeout);
  #if BASESTATION_CONTINUOUS_RX
      radio_receive_continuously();
  #else  /* BASESTATION_CONTINUOUS_RX */
      radio_receive(BS_TASK_WAKEUP_PERIOD_MS * HS_TIMER_FREQUENCY_MS);
  #endif /* BASESTATION_CONTINUOUS_RX */
      last_rx_start = lptimer_now();
      prev_was_tx   = false;
      (void)prev_was_tx;
    }

    /* check the last RX started timestamp */
    if ((lptimer_now() - last_rx_start) > (LPTIMER_SECOND * 7200)) {
      LOG_WARNING("no packet received for >2h (sync cnt: %u)", radio_get_sync_counter());
      radio_print_status();
    }
    /* poll the BOLT and debug tasks */
  #if BOLT_ENABLE
    xTaskNotifyGive(xTaskHandle_bolt);
  #endif /* BOLT_ENABLE */
    xTaskNotifyGive(xTaskHandle_timesync);
    xTaskNotifyGive(xTaskHandle_debug);
  }
}


static void add_command_to_ack(dpp_ack_cmd_t* ack, uint16_t device_id)
{
  if (ack->num_cmds >= DPP_ACK_MAX_CMDS) {
    /* message is full, can't insert any more commands */
    return;
  }
  uint32_t cmd = get_pending_command(device_id);
  if (cmd) {
    LOG_VERBOSE("command %u with arg %u for node %u embedded into the ACK", (uint16_t)(cmd & 0xffff), (uint16_t)(cmd >> 16), device_id);
    ack->commands[ack->num_cmds].cmd32     = cmd;
    ack->commands[ack->num_cmds].target_id = device_id;
    ack->num_cmds++;
    EVENT_INFO(EVENT_SX1262_CMD_FORWARDED, (cmd << 16) | device_id);
  }
}


void basestation_prepare_ack(const dpp_message_t* msg)
{
  static dpp_ack_cmd_t ack_cmd;

  ack_cmd.ack_seq_no = msg->header.seqnr;
  ack_cmd.num_cmds   = 0;

  /* figure out whether the received packet must be acknowledged */
  if (msg->header.type == DPP_MSG_TYPE_GEO_ACQ_AGGR || msg->header.type == DPP_MSG_TYPE_HEALTH_AGGR) {
    /* extract node IDs from the message */
    uint32_t i;
    for (i = 0; i < msg->data_aggr.block_cnt; i++) {
      uint16_t id = 0;
      if (msg->header.type == DPP_MSG_TYPE_GEO_ACQ_AGGR) {
        id = msg->data_aggr.geo_acq_blocks[i].node_id;
      } else if (msg->header.type == DPP_MSG_TYPE_HEALTH_AGGR) {
        id = msg->data_aggr.health_blocks[i].node_id;
      }
      /* check if there are any pending commands for this node */
      add_command_to_ack(&ack_cmd, id);
    }
  } else if (msg->header.type == DPP_MSG_TYPE_HEALTH_MIN || msg->header.type == DPP_MSG_TYPE_NODE_INFO) {
    /* check if there is a pending command for the sender node */
    add_command_to_ack(&ack_cmd, msg->header.device_id);
  } else {
    /* don't send an ACK for other message types */
    if (msg->header.type != DPP_MSG_TYPE_FW) {        // only warn if not msg type FW
      LOG_WARNING("no ACK sent for message of type %u", msg->header.type);
    }
    return;
  }

  /* empty the TX queue to make sure the ACK is sent immediately */
  xQueueReset(xQueueHandle_lora_tx);
  /* insert message into the transmit queue */
  if (!enqueue_message(msg->header.device_id, DPP_MSG_TYPE_ACK_COMMAND, (uint8_t*)&ack_cmd, 0, INTERFACE_LORA)) {
    LOG_WARNING("failed to add ACK to TX queue");
  }
}


void basestation_tx_done(void)
{
  LOG_VERBOSE("TX done");
  /* if more messages are pending, go again into TX mode */
  if (uxQueueMessagesWaitingFromISR(xQueueHandle_lora_tx)) {
    NOTIFY_BS_TASK_FROM_ISR(BS_TASK_NOTIFY_TX);
  } else {
    NOTIFY_BS_TASK_FROM_ISR(BS_TASK_NOTIFY_RX);
  }
  transmitting = false;
}


void basestation_rx_done(uint8_t* payload, uint16_t size, int16_t rssi, int8_t snr, bool crc_error)
{
  dpp_message_t* msg = (dpp_message_t*)payload;

  if (crc_error) {
    LOG_WARNING("CRC error");
    NOTIFY_BS_TASK_FROM_ISR(BS_TASK_NOTIFY_RX);   // go back to RX
    return;
  }

  // calculate an estimate for the RX start timestamp (time when the sync was received minus the txSync offset)
  uint64_t rx_timestamp = get_time(radio_get_last_sync_timestamp() - gloria_timings[BASESTATION_LORA_MODULATION].txSync);
  LOG_VERBOSE("RX done (len: %d  type: %u  sender: %u  RSSI: %d  SNR: %d  timestamp: %llu)", size, msg->header.type, msg->header.device_id, rssi, snr, rx_timestamp);

  if (!(msg->header.type & DPP_MSG_TYPE_MIN)) {
    /* update generation time in case it is relative */
    if (msg->header.generation_time < BS_VALID_GENTIME_REL_THRESHOLD) {
      /* before modifying the packet: make sure the CRC is correct */
      if (msg->header.payload_len <= DPP_MSG_PAYLOAD_LEN) {
        uint16_t calc_crc = crc16(payload, DPP_MSG_LEN(msg) - DPP_MSG_CRC_LEN, 0);
        uint16_t msg_crc  = DPP_MSG_GET_CRC16(msg);
        if (calc_crc != msg_crc) {
          LOG_WARNING("invalid DPP message CRC");
        } else {
          msg->header.generation_time = rx_timestamp - msg->header.generation_time;
          ps_update_msg_crc(msg);
          LOG_INFO("message generation time updated to %llu", msg->header.generation_time);
        }
      } else {
        LOG_WARNING("invalid message length detected (%u bytes)", msg->header.payload_len);
      }
    } else if (msg->header.generation_time < BS_VALID_GENTIME_ABS_THRESHOLD) {
      /* generation time appears to be invalid -> replace with current time */
      LOG_WARNING("invalid message generation time (%llu) replaced", msg->header.generation_time);
      msg->header.generation_time = rx_timestamp;
      ps_update_msg_crc(msg);
    }
  }

  if (!xQueueSendFromISR(xQueueHandle_lora_rx, payload, 0)) {
    LOG_WARNING("failed to insert message into RX queue");
  }

  radio_reset_sync_counter();

  /* notify task to send the ACK */
  NOTIFY_BS_TASK_FROM_ISR(BS_TASK_NOTIFY_TX);
}


void basestation_rx_timeout(bool crc_error)
{
  if (crc_error) {
    LOG_VERBOSE("CRC error");
  } else {
    //LOG_VERBOSE("RX timeout");
  }
  /* go back to RX mode */
  NOTIFY_BS_TASK_FROM_ISR(BS_TASK_NOTIFY_RX);
}


void basestation_do_cad(void)
{
  /* perform CAD */
  uint32_t occupied_cnt = 0,
           free_cnt     = 0;

  LOG_VERBOSE("starting CAD...");

  while (1) {
    if (radio_is_channel_free(BASESTATION_LORA_MODULATION, 200)) {
      free_cnt++;
    } else {
      occupied_cnt++;
    }
    if ((occupied_cnt + free_cnt) % 50 == 0) {
      LOG_VERBOSE("channel was occupied %lu out of %lu times", occupied_cnt, occupied_cnt + free_cnt);
      /* poll the BOLT and debug tasks */
#if BOLT_ENABLE
      xTaskNotifyGive(xTaskHandle_bolt);
#endif /* BOLT_ENABLE */
      xTaskNotifyGive(xTaskHandle_timesync);
      xTaskNotifyGive(xTaskHandle_debug);
    }
    vTaskDelay(pdMS_TO_TICKS(100));
  }
}


void basestation_sample_rssi(void)
{
  int32_t rssi_sum     = 0;
  int32_t rssi_samples = 0;

  /* make sure no receive callback function is set */
  radio_set_rx_callback(0);
  radio_set_timeout_callback(0);

  while (true) {
    /* only works reliably if radio is put into receive mode each time */
    radio_standby();
    radio_receive(0);
    vTaskDelay(pdMS_TO_TICKS(3));      // Wait until receive mode is initialized
    rssi_sum += radio_get_rssi();
    rssi_samples++;
    /* print every ~1s */
    if ((rssi_samples % 100) == 0) {
      LOG_VERBOSE("average RSSI: %lddBm", (rssi_sum / rssi_samples));
      rssi_sum     = 0;
      rssi_samples = 0;
#if BOLT_ENABLE
      xTaskNotifyGive(xTaskHandle_bolt);
#endif /* BOLT_ENABLE */
      xTaskNotifyGive(xTaskHandle_timesync);
      xTaskNotifyGive(xTaskHandle_debug);
    }
    vTaskDelay(pdMS_TO_TICKS(7));    // wait
  }
}

#endif /* BASESTATION */
