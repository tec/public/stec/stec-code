/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "cmsis_os.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
#ifndef CPU_OFF_IND
#define CPU_OFF_IND()
#define CPU_ON_IND()
#endif /* CPU_OFF_IND */
#ifndef IDLE_TASK_RESUMED
#define IDLE_TASK_RESUMED()
#define IDLE_TASK_SUSPENDED()
#endif /* IDLE_TASK_RESUMED */
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* RTOS Task Handles */
#if BOLT_ENABLE
TaskHandle_t xTaskHandle_bolt        = NULL;
#endif /* BOLT_ENABLE */
#if BASESTATION
TaskHandle_t xTaskHandle_basestation = NULL;
#else /* BASESTATION */
TaskHandle_t xTaskHandle_com         = NULL;
#endif /* BASESTATION */
TaskHandle_t xTaskHandle_timesync    = NULL;
TaskHandle_t xTaskHandle_debug       = NULL;
TaskHandle_t xTaskHandle_idle        = NULL;
/* RTOS Queue Handles */
QueueHandle_t xQueueHandle_fsk_rx    = NULL;    /* messages received from the FSK network */
QueueHandle_t xQueueHandle_fsk_tx    = NULL;    /* messages to be transmitted over the FSK network */
QueueHandle_t xQueueHandle_lora_rx   = NULL;    /* messages received over LoRa (from the base station) */
QueueHandle_t xQueueHandle_lora_tx   = NULL;    /* messages to be transmitted over LoRa (to the base station) */
/* RTOS Semaphore Handles */
SemaphoreHandle_t xSemHandle_rtc     = NULL;

/* Variables */
uint64_t active_time      = 0;
uint64_t wakeup_timestamp = 0;
uint64_t last_reset       = 0;

/* USER CODE END Variables */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

extern void task_basestation(void * argument);
extern void task_bolt(void* argument);
extern void task_com(void* argument);
extern void task_debug(void* argument);
extern void task_timesync(void* argument);

/* USER CODE END FunctionPrototypes */

/* Pre/Post sleep processing prototypes */
void PreSleepProcessing(uint32_t *ulExpectedIdleTime);
void PostSleepProcessing(uint32_t *ulExpectedIdleTime);

/* Hook prototypes */
void vApplicationIdleHook(void);
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName);
void vApplicationMallocFailedHook(void);

/* USER CODE BEGIN 2 */
void vApplicationIdleHook( void )
{
   /* vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
   to 1 in FreeRTOSConfig.h. It will be called on each iteration of the idle
   task. It is essential that code added to this hook function never attempts
   to block in any way (for example, call xQueueReceive() with a block time
   specified, or call vTaskDelay()). If the application makes use of the
   vTaskDelete() API function (as this demo application does) then it is also
   important that vApplicationIdleHook() is permitted to return to its calling
   function, because it is the responsibility of the idle task to clean up
   memory allocated by the kernel to any task that has since been deleted. */

  IDLE_TASK_RESUMED();

  if (!xTaskHandle_idle) {
    xTaskHandle_idle = xTaskGetCurrentTaskHandle();
  }

  /* if the application ends up in the RESET state, something went wrong -> reset the MCU */
  if (lpm_get_opmode() == OP_MODE_RESET) {
    NVIC_SystemReset();
  }

  IDLE_TASK_SUSPENDED();
}
/* USER CODE END 2 */

/* USER CODE BEGIN 4 */
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName)
{
   /* Run time stack overflow checking is performed if
   configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2. This hook function is
   called if a stack overflow is detected. */

  (void)pcTaskName;
  (void)xTask;
  EVENT_ERROR(EVENT_SX1262_STACK_WM, 100);
  FATAL_ERROR("--- stack overflow detected! ---\r\n");
}
/* USER CODE END 4 */

/* USER CODE BEGIN 5 */
void vApplicationMallocFailedHook(void)
{
   /* vApplicationMallocFailedHook() will only be called if
   configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h. It is a hook
   function that will get called if a call to pvPortMalloc() fails.
   pvPortMalloc() is called internally by the kernel whenever a task, queue,
   timer or semaphore is created. It is also called by various parts of the
   demo application. If heap_1.c or heap_2.c are used, then the size of the
   heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
   FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
   to query the size of free heap space that remains (although it does not
   provide information on how the remaining heap might be fragmented). */
  FATAL_ERROR("--- malloc failed ---\r\n");
}
/* USER CODE END 5 */

/* USER CODE BEGIN PREPOSTSLEEP */
void PreSleepProcessing(uint32_t *ulExpectedIdleTime)
{
  /* interrupts are disabled within this function */

  /* note: for tickless idle, the HAL tick needs to be suspended! */
  lpm_prepare();

  /* duty cycle measurement */
  active_time += lptimer_now() - wakeup_timestamp;
  CPU_OFF_IND();
}

void PostSleepProcessing(uint32_t *ulExpectedIdleTime)
{
  /* interrupts are disabled within this function */

  CPU_ON_IND();
  wakeup_timestamp = lptimer_now();    /* reset duty cycle timer */

  lpm_resume();
}
/* USER CODE END PREPOSTSLEEP */

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
/* RTOS functions ------------------------------------------------------------*/
void rtos_init(void)
{
  /* create RTOS queues */
  xQueueHandle_fsk_rx = xQueueCreate(FSK_RX_QUEUE_SIZE, DPP_MSG_PKT_LEN);   // queue ID 1
  if (xQueueHandle_fsk_rx == NULL) {
    Error_Handler();
  }
  xQueueHandle_fsk_tx = xQueueCreate(FSK_TX_QUEUE_SIZE, DPP_MSG_PKT_LEN);   // queue ID 2
  if (xQueueHandle_fsk_tx == NULL) {
    Error_Handler();
  }
  xQueueHandle_lora_rx = xQueueCreate(LORA_RX_QUEUE_SIZE, DPP_MSG_PKT_LEN); // queue ID 3
  if (xQueueHandle_lora_rx == NULL) {
    Error_Handler();
  }
  xQueueHandle_lora_tx = xQueueCreate(LORA_TX_QUEUE_SIZE, DPP_MSG_PKT_LEN); // queue ID 4
  if (xQueueHandle_lora_tx == NULL) {
    Error_Handler();
  }

  /* create RTOS semaphores */
  xSemHandle_rtc = xSemaphoreCreateBinary();
  if (xSemHandle_rtc == NULL) {
    Error_Handler();
  }
  xSemaphoreGive(xSemHandle_rtc);

  /* create RTOS tasks */
  /* max. priority is (configMAX_PRIORITIES - 1), higher numbers = higher priority; idle task has priority 0 */
#if BOLT_ENABLE
  if (xTaskCreate(task_bolt,
                  "boltTask",
                  BOLT_TASK_STACK_SIZE,
                  NULL,
                  tskIDLE_PRIORITY + 2,
                  &xTaskHandle_bolt) != pdPASS) { Error_Handler(); }
#endif /* BOLT_ENABLE */
  if (xTaskCreate(task_timesync,
                  "timesyncTask",
                  TIMESYNC_TASK_STACK_SIZE,
                  NULL,
                  tskIDLE_PRIORITY + 2,
                  &xTaskHandle_timesync) != pdPASS) { Error_Handler(); }
  if (xTaskCreate(task_debug,
                  "debugTask",
                  DEBUG_TASK_STACK_SIZE,
                  NULL,
                  tskIDLE_PRIORITY + 1,            /* lowest priority right after the idle task */
                  &xTaskHandle_debug) != pdPASS) { Error_Handler(); }

#if BASESTATION
  if (xTaskCreate(task_basestation,
                  "baseStationTask",
                  BASESTATION_TASK_STACK_SIZE,
                  NULL,
                  configMAX_PRIORITIES - 1,        /* highest priority task */
                  &xTaskHandle_basestation) != pdPASS)  { Error_Handler(); }
#else /* BASESTATION */
  if (xTaskCreate(task_com,
                  "comTask",
                  COM_TASK_STACK_SIZE,
                  NULL,
                  configMAX_PRIORITIES - 1,        /* highest priority task */
                  &xTaskHandle_com) != pdPASS) { Error_Handler(); }
#endif /* BASESTATION */
}

uint32_t rtos_get_cpu_dc(void)
{
  uint32_t elapsed = (lptimer_now() - last_reset);
  if (elapsed > 0) {
    return (uint32_t)((active_time * 10000) / elapsed);
  } else {
    return 10000;
  }
}

void rtos_reset_cpu_dc(void)
{
  last_reset = lptimer_now();
  active_time = 0;
}

/* returns the highest watermark value */
uint32_t rtos_check_stack_usage(void)
{
  static unsigned long idleTaskStackWM = 0,
#if BOLT_ENABLE
                       boltTaskStackWM = 0,
#endif /* BOLT_ENABLE */
#if BASESTATION
                       baseStationTaskStackWM = 0,
#else
                       comTaskStackWM  = 0,
#endif /* BASESTATION */
                       tsyncTaskStackWM = 0,
                       debugTaskStackWM = 0;

  /* check stack watermarks (store the used words) */

  unsigned long wm = configMINIMAL_STACK_SIZE - (uxTaskGetStackHighWaterMark(xTaskHandle_idle));
  unsigned long usage;
  if (wm > idleTaskStackWM) {
    idleTaskStackWM = wm;
    usage = idleTaskStackWM * 100 / configMINIMAL_STACK_SIZE;
    if (usage > STACK_WARNING_THRESHOLD) {
      LOG_WARNING("stack watermark of idle task reached %u%%", usage);
      EVENT_WARNING(EVENT_SX1262_STACK_WM, usage);
    } else {
      LOG_INFO("stack watermark of idle task increased to %u%%", usage);
    }
  } else {
    usage = idleTaskStackWM * 100 / configMINIMAL_STACK_SIZE;
  }
  unsigned long wm_max = usage;

#if BOLT_ENABLE
  wm = BOLT_TASK_STACK_SIZE - (uxTaskGetStackHighWaterMark(xTaskHandle_bolt));
  if (wm > boltTaskStackWM) {
    boltTaskStackWM = wm;
    usage = boltTaskStackWM * 100 / BOLT_TASK_STACK_SIZE;
    if (usage > STACK_WARNING_THRESHOLD) {
      LOG_WARNING("stack watermark of bolt task reached %u%%", usage);
      EVENT_WARNING(EVENT_SX1262_STACK_WM, 0x00010000 | usage);
    } else {
      LOG_INFO("stack watermark of bolt task increased to %u%%", usage);
    }
  } else {
    usage = boltTaskStackWM * 100 / BOLT_TASK_STACK_SIZE;
  }
  if (usage > wm_max) {
    wm_max = usage;
  }
#endif /* BOLT_ENABLE */

#if BASESTATION
  wm = BASESTATION_TASK_STACK_SIZE - (uxTaskGetStackHighWaterMark(xTaskHandle_basestation));
  if (wm > baseStationTaskStackWM) {
    baseStationTaskStackWM = wm;
    usage = baseStationTaskStackWM * 100 / BASESTATION_TASK_STACK_SIZE;
    if (usage > STACK_WARNING_THRESHOLD) {
      LOG_WARNING("stack watermark of basestation task reached %u%%", usage);
      EVENT_WARNING(EVENT_SX1262_STACK_WM, 0x00050000 | usage);
    } else {
      LOG_INFO("stack watermark of basestation task increased to %u%%", usage);
    }
  } else {
    usage = baseStationTaskStackWM * 100 / BASESTATION_TASK_STACK_SIZE;
  }
  if (usage > wm_max) {
    wm_max = usage;
  }
#else
  wm = COM_TASK_STACK_SIZE - (uxTaskGetStackHighWaterMark(xTaskHandle_com));
  if (wm > comTaskStackWM) {
    comTaskStackWM = wm;
    usage = comTaskStackWM * 100 / COM_TASK_STACK_SIZE;
    if (usage > STACK_WARNING_THRESHOLD) {
      LOG_WARNING("stack watermark of com task reached %u%%", usage);
      EVENT_WARNING(EVENT_SX1262_STACK_WM, 0x00020000 | usage);
    } else {
      LOG_INFO("stack watermark of com task increased to %u%%", usage);
    }
  } else {
    usage = comTaskStackWM * 100 / COM_TASK_STACK_SIZE;
  }
  if (usage > wm_max) {
    wm_max = usage;
  }
#endif /* BASESTATION */

  wm = DEBUG_TASK_STACK_SIZE - (uxTaskGetStackHighWaterMark(xTaskHandle_debug));
  if (wm > debugTaskStackWM) {
    debugTaskStackWM = wm;
    usage = debugTaskStackWM * 100 / DEBUG_TASK_STACK_SIZE;
    if (usage > STACK_WARNING_THRESHOLD) {
      LOG_WARNING("stack watermark of debug task reached %u%%", usage);
      EVENT_WARNING(EVENT_SX1262_STACK_WM, 0x00040000 | usage);
    } else {
      LOG_INFO("stack watermark of debug task increased to %u%%", usage);
    }
  } else {
    usage = debugTaskStackWM * 100 / DEBUG_TASK_STACK_SIZE;
  }
  if (usage > wm_max) {
    wm_max = usage;
  }

  wm = TIMESYNC_TASK_STACK_SIZE - (uxTaskGetStackHighWaterMark(xTaskHandle_timesync));
  if (wm > tsyncTaskStackWM) {
    tsyncTaskStackWM = wm;
    usage = tsyncTaskStackWM * 100 / TIMESYNC_TASK_STACK_SIZE;
    if (usage > STACK_WARNING_THRESHOLD) {
      LOG_WARNING("stack watermark of timesync task reached %u%%", usage);
      EVENT_WARNING(EVENT_SX1262_STACK_WM, 0x00030000 | usage);
    } else {
      LOG_INFO("stack watermark of timesync task increased to %u%%", usage);
    }
  } else {
    usage = tsyncTaskStackWM * 100 / TIMESYNC_TASK_STACK_SIZE;
  }
  if (usage > wm_max) {
    wm_max = usage;
  }

  /* return the highest watermark value */
  return wm_max;
}

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
