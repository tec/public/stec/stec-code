/*
 * Copyright (c) 2020 - 2021, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * STeC source node communication task
 */

#include "main.h"


#if !BASESTATION

/* Typedefs */
typedef enum
{
  COM_STATE_IDLE,                             // Idle, wait for a new event (no events are currently running)
  COM_STATE_STAGGERED_WAKEUP,                 // Event is detected, wait a given time so that all nodes in an area can detect the event, node is in RX mode
  COM_STATE_WAKEUP,                           // Check if channel is free (this state is only entered if duty-cycled CAD has been used for staggered wakeup)
  COM_STATE_SEND_WAKEUP_MESSAGE,              // Send a wake-up message
  COM_STATE_WAIT_FOR_LEADER_ELECTION,         // Wait until the leader election starts
  COM_STATE_LEADER_ELECTION,                  // Leader Election, elects a unique node (leader) which can then start the next round
  COM_STATE_WAIT_FOR_DATA_AGGREGATION,        // Wait until the data aggregation starts
  COM_STATE_DATA_AGGREGATION,                 // Aggregates all the data from all active nodes
  COM_STATE_SEND_TO_BS,                       // Send aggregated data to the base station
  COM_STATE_WAIT_FOR_BS_ACK,                  // Wait for an Ack from the BS
  COM_STATE_WAIT_FOR_COMMAND,                 // Wait until command distribution round starts
  COM_STATE_PROCESS_COMMAND,                  // Distribute / receive and process commands
  NUM_COM_STATES,
} com_state_t;

typedef enum
{
  WAKEUP_MODE_FSK,                              // Listen constantly in RX mode during the staggered wakeup
  WAKEUP_MODE_LORA_CAD,                         // Use the CAD mode (Carrier Activity Detection) with timeout
  WAKEUP_MODE_LORA_DC_CAD,                      // Use duty-cycled CAD to detect a message
  WAKEUP_MODE_DISCOSYNC,                        // Use discovery sync during staggered wakeup
} wakeup_mode_t;

typedef enum
{
  LEADER_ELECTION_MODE_LINEAR = 0,              // Go from MAX_ID to 0
  LEADER_ELECTION_MODE_BINARY_SEARCH = 1,       // Checks each NODE_ID bit, if it is a 1 then start transmitting, otherwise listen
  LEADER_ELECTION_MODE_BASE_2 = 2,              // 8 rounds with 2 slots each
  LEADER_ELECTION_MODE_BASE_4 = 4,              // 4 rounds with 4 slots each
  LEADER_ELECTION_MODE_BASE_8 = 8,              // 3 rounds with 8 slots each
  LEADER_ELECTION_MODE_BASE_10 = 10,            // 3 rounds with 10 slots each
  LEADER_ELECTION_MODE_BASE_16 = 16,            // 2 rounds with 16 slots each
} leader_election_mode_t;

typedef struct
{
  uint32_t    hop_cnt_max;
  int32_t     rssi_sum;
  uint32_t    snr_sum;
  uint32_t    num_samples;
} radio_stats_t;

/* Macros */
#define END_EVENT()                 notify_com_task(COM_TASK_NOTIFY_ABORT, true, false)
#define END_EVENT_FROM_ISR()        notify_com_task(COM_TASK_NOTIFY_ABORT, true, true)
#define NOTIFY_COM_TASK()           notify_com_task(COM_TASK_NOTIFY_CONTINUE, true, false)
#define NOTIFY_COM_TASK_FROM_ISR()  notify_com_task(COM_TASK_NOTIFY_CONTINUE, true, true)

#ifndef COM_TASK_RESUMED
#define COM_TASK_RESUMED()
#define COM_TASK_SUSPENDED()
#endif /* COM_TASK_RESUMED */

#ifndef COM_TASK_PHASE_START_IND
#define COM_TASK_PHASE_START_IND()
#define COM_TASK_PHASE_STOP_IND()
#endif /* COM_TASK_PHASE_START_IND */

#define LEADER_ELECTION_MODE_BASE   ((LEADER_ELECTION_MODE > 1) ? LEADER_ELECTION_MODE : 0)

/* External variables */
extern TaskHandle_t       xTaskHandle_com;
extern TaskHandle_t       xTaskHandle_bolt;
extern TaskHandle_t       xTaskHandle_debug;
extern TaskHandle_t       xTaskHandle_timesync;
extern QueueHandle_t      xQueueHandle_fsk_rx;
extern QueueHandle_t      xQueueHandle_fsk_tx;
extern QueueHandle_t      xQueueHandle_lora_rx;
extern QueueHandle_t      xQueueHandle_lora_tx;
extern uint8_t            codetection_th;

/* Global config */
bool                      abort_event_on_rtc_trigger = ABORT_EVENT_ON_RTC_WAKEUP;
uint8_t                   send_bs_modulation         = SEND_BS_LORA_MODULATION;
int8_t                    send_bs_tx_pwr             = SEND_BS_LORA_TX_POWER;
bool                      send_bs_cad_mode           = true;
uint32_t                  send_bs_fail_cnt           = 0;
uint16_t                  stag_wakeup_timeout_ms     = STAG_WAKEUP_TIMEOUT_MS;

/* Internal state */
static com_state_t        com_state;
static bool               is_leader;
static bool               is_alone;
static bool               is_reporter;
static bool               trg_pending;
static bool               skip_first_tx;
static uint8_t            msg_buffer[GLORIA_INTERFACE_MAX_PAYLOAD_LEN];
static radio_stats_t      radio_stats = { 0 };
static uint64_t           ref_time = 0;

/* Function prototypes */
void set_rx_tx_config(void);
void start_rx(void);
bool is_channel_free(void);
void send_wakeup_message(void);
void send_to_basestation(uint32_t n_tries);
void on_radio_tx_done(void);
void on_radio_rx_done(uint8_t* payload, uint16_t size, int16_t rssi, int8_t snr, bool crc_error);
void on_radio_cad_done(bool detected);
void on_radio_timeout(bool crc_error);
void wakeup_flood_finished(void);
void discosync_finished(void);
void leader_election(void);
bool check_if_sending_slot(uint16_t slot_counter, uint8_t round_counter);
uint16_t calc_nodeid_diff(uint16_t id1, uint16_t id2);
void set_wakeup_rx_mode(void);
void distribute_ack(void);
bool collect_data(void);
void aggregate_data(bool health_data);
void timeout_callback(void);
void schedule_timeout(uint64_t timestamp, uint32_t ms);
uint32_t ms_until_timeout(void);
void cancel_timeout(void);
void delay_com_task(uint32_t ms);
void collect_radio_stats(void);


/* ----- Task Definition ----- */

void task_com(void const * argument)
{
  bool     is_health = false;
  uint32_t n_retries = 0;

#if SEND_NODE_INFO_MSG
  /* send the node info message */
  generate_node_info();
  com_state     = COM_STATE_SEND_TO_BS;
  is_alone      = true;
  is_reporter   = true;
  skip_first_tx = false;
 #if FLOCKLAB
  /* on FlockLab, add a startup delay to avoid collisions with other nodes */
  vTaskDelay(pdMS_TO_TICKS(NODE_ID * 1500));
 #endif /* FLOCKLAB */

#else
  com_state = COM_STATE_IDLE;
#endif /* SEND_NODE_INFO_MSG */
  NOTIFY_COM_TASK();

  LOG_VERBOSE("com task started");
  LOG_VERBOSE("network size: %u nodes (IDs %u - %u)", MAX_NUM_NODES, MIN_NODE_ID, MIN_NODE_ID + MAX_NUM_NODES - 1);

  for (;;) {
    /* wait for a new event (see com_task_notify_t for possible notification values) */
    uint32_t notification;
    COM_TASK_SUSPENDED();
    xTaskNotifyWait(0, ULONG_MAX, &notification, portMAX_DELAY);     // note:  ulTaskNotifyTake() waits for a non-zero value!
    //LOG_VERBOSE("notification value %u received (current state is %u)", notification, com_state);
    COM_TASK_RESUMED();

    switch (notification) {

    case COM_TASK_NOTIFY_ABORT:
      /* abort, back to idle state */
      com_state = COM_STATE_IDLE;
      break;

    case COM_TASK_NOTIFY_TRIGGER:
      if (com_state == COM_STATE_IDLE) {
        com_state = COM_STATE_STAGGERED_WAKEUP;
        LOG_INFO("trigger received");
      } else {
        if (com_state > COM_STATE_WAIT_FOR_DATA_AGGREGATION) {
          LOG_INFO("re-trigger received");
          trg_pending = true;
        } else {
          LOG_WARNING("trigger ignored");
        }
        /* check how long ago the event started */
        uint32_t elapsed_secs = LPTIMER_TICKS_TO_S(lptimer_now() - get_captured_timestamp());
        if (elapsed_secs > EVENT_TIMEOUT_ON_RETRIGGER) {
          /* assume something went wrong and force task back to idle state */
          LOG_WARNING("event timeout (%lus)", elapsed_secs);
          EVENT_WARNING(EVENT_SX1262_TIMEOUT, com_state);
          com_state = COM_STATE_IDLE;
        } else {
          if ((com_state == COM_STATE_WAIT_FOR_DATA_AGGREGATION || com_state == COM_STATE_WAIT_FOR_COMMAND) && ms_until_timeout() > LPM_MIN_IDLE_TIME_MS) {
            lpm_update_opmode(OP_MODE_EVT_DONE);    // go back to LPM
          }
          continue;   // wait for the next notification
        }
      }
      is_health = false;
      break;

    case COM_TASK_NOTIFY_RTC:
      if (com_state != COM_STATE_IDLE) {
        if (!abort_event_on_rtc_trigger) {
          uint32_t elapsed_secs = LPTIMER_TICKS_TO_S(lptimer_now() - get_captured_timestamp());
          if (elapsed_secs < EVENT_TIMEOUT_ON_RETRIGGER) {
            continue;     // ignore trigger
          }
          /* assume something went wrong and force task back to idle state */
          LOG_WARNING("event timeout (%lus)", elapsed_secs);
          EVENT_WARNING(EVENT_SX1262_TIMEOUT, com_state);
        }
        cancel_timeout();
        radio_standby();
      }
      LOG_INFO("RTC trigger");
      is_health     = true;
      is_alone      = true;
      is_reporter   = true;
      skip_first_tx = false;
      n_retries     = 0;
#if AGGREGATE_HEALTH_MESSAGES
      com_state = COM_STATE_STAGGERED_WAKEUP;
#else /* AGGREGATE_HEALTH_MESSAGES */
  #if BOLT_ENABLE
      xTaskNotifyGive(xTaskHandle_bolt);    // yield and let BOLT task run (to read the latest app health message)
      vTaskDelay(pdMS_TO_TICKS(BOLT_TASK_YIELD_TIME_MS));
  #endif /* BOLT_ENABLE */
      xQueueReset(xQueueHandle_lora_tx);    // make sure queue is empty before inserting the health message
      generate_node_health_min(INTERFACE_LORA);
      com_state   = COM_STATE_SEND_TO_BS;
#endif /* AGGREGATE_HEALTH_MESSAGES */
      break;

    default:
      break;
    }

    switch (com_state) {

    case COM_STATE_IDLE:
      LOG_VERBOSE("idle");
      COM_TASK_PHASE_STOP_IND();
      cancel_timeout();
      radio_standby();
      /* reset state */
      trg_pending = false;
      is_reporter = false;
      is_leader   = false;
      is_health   = false;
      is_alone    = true;
      n_retries   = 0;
      ref_time    = 0;
      /* poll the timesync and debug tasks */
      xTaskNotifyGive(xTaskHandle_timesync);
      xTaskNotifyGive(xTaskHandle_debug);
      /* signal to low-power state machine that all work is done */
      lpm_update_opmode(OP_MODE_EVT_DONE);
      break;

    case COM_STATE_STAGGERED_WAKEUP:
      COM_TASK_PHASE_START_IND();
      /* transition to next state will be handled by the timeout function or RX done */
      schedule_timeout(0, stag_wakeup_timeout_ms);
      start_rx();
#if BOLT_ENABLE
      /* let the BOLT task run to read all pending messages that were generated before the current event
       * NOTE: staggered wakeup time must be sufficiently long such that the BOLT task has enough time to read all messages */
      xTaskNotifyGive(xTaskHandle_bolt);
#endif /* BOLT_ENABLE */
      break;

    case COM_STATE_WAKEUP:
      /* check if channel is still free before sending the wakeup message in the next phase */
      start_rx();
      break;

    case COM_STATE_SEND_WAKEUP_MESSAGE:
      send_wakeup_message();
      break;

    case COM_STATE_WAIT_FOR_LEADER_ELECTION:
      COM_TASK_PHASE_STOP_IND();
#if !LEADER_ELECTION_ALIGN_SLOTS
      // NOTE: if leader election slots are to be aligned (by using the hs timer), sleep mode must be skipped here
      if (ms_until_timeout() >= LPM_MIN_IDLE_TIME_MS) {
        update_opmode(OP_MODE_EVT_DONE);
      }
#endif /* LEADER_ELECTION_ALIGN_SLOTS */
      break;

    case COM_STATE_LEADER_ELECTION:
      /* at this point, all nodes in the ad-hoc network should be roughly synchronized */
      if (is_health) {
        com_state = COM_STATE_DATA_AGGREGATION;   // no need to wait, go straight to data aggregation
      } else {
        com_state = COM_STATE_WAIT_FOR_DATA_AGGREGATION;
        schedule_timeout(0, DATA_AGGREGATION_DELAY_MS - LEADER_ELECTION_OFFSET_MS - DATA_AGGREGATION_GUARD_TIME_MS);
      }
      COM_TASK_PHASE_START_IND();
      leader_election();
      COM_TASK_PHASE_STOP_IND();
      /* make sure the FSK TX queue is empty (potentially contains old acquisition messages from past events)
       * NOTE: staggered wakeup duration must not exceed minimal event duration of the geophone */
      xQueueReset(xQueueHandle_fsk_tx);
      NOTIFY_COM_TASK();
      break;

    case COM_STATE_WAIT_FOR_DATA_AGGREGATION:
      n_retries = 0;    // needs to be reset to 0 in case there are several data aggregation rounds
      lpm_update_opmode(OP_MODE_EVT_DONE);
      break;

    case COM_STATE_DATA_AGGREGATION:
      COM_TASK_PHASE_START_IND();
      aggregate_data(is_health);
      COM_TASK_PHASE_STOP_IND();
      break;

    case COM_STATE_SEND_TO_BS:
      COM_TASK_PHASE_START_IND();
      if (n_retries < SEND_BS_TRIES) {
        n_retries++;
        send_to_basestation(n_retries);
      } else {
        LOG_WARNING("TX to base failed");
        EVENT_WARNING(EVENT_SX1262_TX_FAILED, 0);
        send_bs_fail_cnt++;
        xQueueReset(xQueueHandle_lora_tx);    // clear the TX queue
        schedule_timeout(ref_time, DISTR_ACKCMD_DELAY_MS);
        com_state = COM_STATE_WAIT_FOR_COMMAND;
        COM_TASK_PHASE_STOP_IND();
        NOTIFY_COM_TASK();
      }
      break;

    case COM_STATE_WAIT_FOR_BS_ACK:
      start_rx();
      break;

    case COM_STATE_WAIT_FOR_COMMAND:
      COM_TASK_PHASE_STOP_IND();
      if (is_alone) {
        /* no need to distribute the ACK, the node is alone -> process the received ACK and end the event */
        if (xQueueReceive(xQueueHandle_lora_rx, msg_buffer, 0)) {
          process_message((dpp_message_t*)msg_buffer, 0);
        }
        END_EVENT();    // abort
      } else {
        lpm_update_opmode(OP_MODE_EVT_DONE);
      }
      break;

    case COM_STATE_PROCESS_COMMAND:
      COM_TASK_PHASE_START_IND();
      distribute_ack();
      COM_TASK_PHASE_STOP_IND();
      break;

    default:
      break;
    }
  }
}


/*================ Tx/Rx Functions ================*/

void start_rx(void)
{
  set_rx_tx_config();   // also sets radio callback functions

  switch (com_state) {

  case COM_STATE_STAGGERED_WAKEUP:
    set_wakeup_rx_mode();
    break;

  case COM_STATE_WAKEUP:
    /* perform CAD to check if channel is free */
    radio_set_cad(STAG_WAKEUP_LORA_MODULATION, true, false);
    break;

  case COM_STATE_WAIT_FOR_BS_ACK:
    /* go to receive mode with timeout (for when no ack is received) */
    schedule_timeout(0, SEND_BS_RX_ACK_TIMEOUT_MS);
    radio_receive(0);
    break;

  default:
    END_EVENT();
    break;
  }
}


void set_wakeup_rx_mode(void)
{
  switch (STAG_WAKEUP_MODE) {

  case WAKEUP_MODE_FSK:
    /* start receiving */
    gloria_register_flood_callback(wakeup_flood_finished);
    gloria_start(false, msg_buffer, 0, GLORIA_INTERFACE_N_TX, 1);
    break;

  case WAKEUP_MODE_LORA_CAD:
    /* start carrier activity detection to detect wakeup message */
    radio_set_cad(STAG_WAKEUP_LORA_MODULATION, true, true);
    break;

  case WAKEUP_MODE_LORA_DC_CAD:
    LOG_WARNING("WAKEUP_MODE_LORA_DC_CAD not implemented");
    //TODO implement
    break;

  case WAKEUP_MODE_DISCOSYNC:
  {
    cancel_timeout();   // discosync does not require a timeout
    discosync_start(DISCOSYNC_NUM_SLOTS, STAG_WAKEUP_TIMEOUT_MS, discosync_finished);
  } break;

  default:
    END_EVENT();
    break;
  }
}


void send_wakeup_message(void)
{
  dpp_message_min_t* wakeup_msg  = (dpp_message_min_t*)msg_buffer;
  wakeup_msg->header.device_id   = config.node_id;
  wakeup_msg->header.type        = DPP_MSG_TYPE_STAG_WAKEUP;
  wakeup_msg->header.payload_len = 0;
  uint32_t msg_len = DPP_MSG_MIN_LEN(wakeup_msg);
  ps_update_msg_crc((dpp_message_t*)wakeup_msg);

  /* transition to leader election state will be handled by the timeout function */
  schedule_timeout(0, LEADER_ELECTION_OFFSET_MS);
  if (STAG_WAKEUP_MODE == WAKEUP_MODE_FSK) {
    /* use FSK to transmit the wake-up message */
    gloria_start(true, msg_buffer, msg_len, GLORIA_INTERFACE_N_TX, 1);
    vTaskDelay(pdMS_TO_TICKS(STAG_WAKEUP_SLOT_MS));
    gloria_stop();
    com_state = COM_STATE_WAIT_FOR_LEADER_ELECTION;

  } else {
    /* use LoRa to transmit the wake-up message */
    set_rx_tx_config();
    radio_transmit(msg_buffer, msg_len);
    /* transition to COM_STATE_WAIT_FOR_LEADER_ELECTION will be handled by TX done interrupt */
  }

  NOTIFY_COM_TASK();
}


void send_to_basestation(uint32_t n_tries)
{
  /* make sure the RX queue is empty */
  xQueueReset(xQueueHandle_lora_rx);

  /* short random delay to avoid collisions*/
  vTaskDelay(pdMS_TO_TICKS(rand() % SEND_BS_RAND_BACKOFF_MS));

  /* check if some data is in the transmit queue */
  if (xQueuePeek(xQueueHandle_lora_tx, msg_buffer, 0)) {
    dpp_message_t* msg = (dpp_message_t*)msg_buffer;
    set_rx_tx_config();

    if (!skip_first_tx && is_channel_free()) {
      /* send message to the base station */
      uint8_t msg_len = DPP_MSG_LEN(msg);
      LOG_VERBOSE("sending message of size %u bytes to the base station", msg_len);
      if (msg->header.type == DPP_MSG_TYPE_GEO_ACQ_AGGR) {
        /* replace the generation time with the elapsed time since the reference time */
        uint32_t elapsed_time_us    = LPTIMER_TICKS_TO_US(lptimer_now() - ref_time);
        msg->header.generation_time = elapsed_time_us - (get_drift_comp_us(elapsed_time_us)) + SEND_BS_TIMESTAMP_OFS_US;  // -> relative time will be converted on base station
      } else {
        /* update the generation time */
        msg->header.generation_time = get_time(0);
      }
      ps_update_msg_crc(msg);
      radio_transmit(msg_buffer, msg_len);

    } else {
      skip_first_tx = false;
      LOG_VERBOSE("channel is occupied");
      if (n_tries < SEND_BS_TRIES) {
        /* channel is occupied, wait for the duration of one packet + ACK */
        schedule_timeout(0, SEND_BS_MIN_BACKOFF_MS);
        lpm_update_opmode(OP_MODE_EVT_DONE);    // go to LPM
      } else {
        /* abort, notify task to continue with state machine */
        NOTIFY_COM_TASK();
      }
    }
  } else {
    LOG_WARNING("TX queue is empty");
  }
}


bool collect_data(void)
{
  uint32_t payload_len  = 0;
  uint16_t reporter_id  = 0;
  bool     is_initiator = 0;
  bool     wrong_leader = false;

#define SCHEDULE_LEN  6

  skip_first_tx = false;
  is_reporter   = true;    // assume to be the reporter

  /* make sure the FSK RX queue is empty */
  xQueueReset(xQueueHandle_fsk_rx);

  /* NOTE: in principle, a schedule packet is not necessary if the node list is constant - however, we use it for tight time synchronisation */
  if (is_leader) {
    /* compose a schedule packet */
    is_initiator = true;
    payload_len  = SCHEDULE_LEN;               // 2 bytes sender ID, dummy message type, 2 bytes for the reporter ID
    /* determine the new leader */
    reporter_id  = MIN_NODE_ID + (rand() % MAX_NUM_NODES);    // select a reporter ID randomly from all possible node IDs
    *(uint16_t*)msg_buffer         = config.node_id;
    *(((uint16_t*)msg_buffer) + 1) = DPP_MSG_TYPE_INVALID;    // to make sure this packet is not accidentally interpreted as a regular DPP message
    *(((uint16_t*)msg_buffer) + 2) = reporter_id;
    LOG_VERBOSE("node %u chosen as reporter", reporter_id);
    vTaskDelay(pdMS_TO_TICKS(DATA_AGGREGATION_GUARD_TIME_MS));
  }
  /* send / receive the schedule packet */
  gloria_start(is_initiator, msg_buffer, payload_len, GLORIA_INTERFACE_N_TX, 1);
  vTaskDelay(pdMS_TO_TICKS(DATA_AGGREGATION_SLOT_TIME_MS));
  gloria_stop();
  /* schedule received? */
  if (!is_leader) {
    payload_len = gloria_get_payload_len();
    if (gloria_get_rx_cnt() == 0 || payload_len != SCHEDULE_LEN || *(((uint16_t*)msg_buffer) + 1) != DPP_MSG_TYPE_INVALID) {
      /* can't continue, as we missed the schedule */
      LOG_WARNING("failed to receive schedule packet");
      EVENT_WARNING(EVENT_SX1262_SCHED_MISSED, 0);
      END_EVENT();
      return false;
    }
    collect_radio_stats();
    reporter_id = *(((uint16_t*)msg_buffer) + 2);
    LOG_VERBOSE("data aggregation sync packet received from leader %u (assigned reporter is %u)", *(uint16_t*)msg_buffer, reporter_id);
  }
  ref_time = gloria_get_t_ref();
  uint64_t t_start_next = ref_time;

  /* send / receive data packets (collect the data from all the participating nodes in the ad-hoc network) */
  uint32_t i;
  const uint32_t my_diff = calc_nodeid_diff(config.node_id, reporter_id);
  for (i = 0; i < MAX_NUM_NODES; i++) {
    is_initiator = false;
    payload_len  = 0;
    int32_t curr_id = MIN_NODE_ID + i;
    if (curr_id == config.node_id) {
      /* send */
      if (xQueueReceive(xQueueHandle_fsk_tx, msg_buffer, 0)) {   // get the next message from the transmit queue
        dpp_message_t* msg = (dpp_message_t*)msg_buffer;
        payload_len  = DPP_MSG_LEN(msg);
        is_initiator = true;
        /* for acquisition events: adjust the start time (relative to the sync point / reference time) before sending the message */
        if (msg->header.type == DPP_MSG_TYPE_GEOPHONE_ACQ) {
          int32_t t_start_rel_ms = (int32_t)((int64_t)get_reference_timestamp() - (int64_t)get_captured_timestamp());
          t_start_rel_ms = (t_start_rel_ms * 1000 + (LPTIMER_SECOND / 2)) / LPTIMER_SECOND;  // convert to ms
          if (t_start_rel_ms < 0 || t_start_rel_ms > 0xffff) {
            t_start_rel_ms = 0xffff;
          }
          msg->geo_acq.start_time = t_start_rel_ms;
          ps_update_msg_crc(msg);
        }
        /* insert the message into the receive queue */
        xQueueSend(xQueueHandle_fsk_rx, msg_buffer, 0);
      } else {
        LOG_WARNING("FSK transmit queue is empty");
      }
    }
    /* synchronize */
    t_start_next  += LPTIMER_MS_TO_TICKS(DATA_AGGREGATION_SLOT_TIME_MS + DATA_AGGREGATION_GAP_TIME_MS);
    uint64_t t_now = lptimer_now();
    if (t_now < t_start_next) {
      delay_us(LPTIMER_TICKS_TO_US(t_start_next - t_now));    // use busy wait instead of vTaskDelay (only has millisecond granularity)
    }
    gloria_start(is_initiator, msg_buffer, payload_len, GLORIA_INTERFACE_N_TX, 0);
    vTaskDelay(pdMS_TO_TICKS(DATA_AGGREGATION_SLOT_TIME_MS));
    gloria_stop();
    /* append to receive queue */
    if ((curr_id != config.node_id) && gloria_get_rx_cnt()) {
      /* check if sender ID and message type is as expected */
      uint16_t sender_id = ((dpp_message_t*)msg_buffer)->header.device_id;
      uint16_t msg_type  = ((dpp_message_t*)msg_buffer)->header.type;
      //LOG_VERBOSE("data received in slot %u from node %u", curr_id, sender_id);
      if (msg_type == DPP_MSG_TYPE_HEALTH_MIN || msg_type == DPP_MSG_TYPE_GEOPHONE_ACQ) {
        if (curr_id == sender_id) {
          xQueueSend(xQueueHandle_fsk_rx, msg_buffer, 0);
          /* check whether our node ID is closest to the announced reporter ID */
          uint32_t other_diff = calc_nodeid_diff(curr_id, reporter_id);
          if (!wrong_leader && is_reporter && ((other_diff < my_diff) || ((other_diff == my_diff) && (curr_id > config.node_id)))) {    // if two IDs are equally close, choose the larger ID
            is_reporter = false;
          }
          is_alone = false;
          if (is_leader && curr_id > config.node_id) {
            /* node with higher ID detected! -> this node is not the leader! */
            LOG_WARNING("node %u has a higher ID and should be the leader", curr_id);
            EVENT_WARNING(EVENT_SX1262_HOST_ID_ERROR, curr_id);
            is_leader    = false;
            is_reporter  = true; /* we must assume that the other leader has elected this node as leader, hence we must also report */
            wrong_leader = true;
          }
        } else {
          LOG_WARNING("sender ID (%u) does not match the slot ID (%u)", sender_id, curr_id);    // something went wrong -> probably two overlapping data collection round of two subnets
          EVENT_WARNING(EVENT_SX1262_MSG_IGNORED, ((uint32_t)msg_type << 16) | sender_id);
        }
      }
      collect_radio_stats();
    }
  }

  if (is_reporter) {
    LOG_VERBOSE("is reporter");
  }
  if (wrong_leader) {
    /* wrong leader is also a reporter: postpone the packet transmission by the length of one packet + ACK in case there is another reporter in the network */
    skip_first_tx = true;
  }

  return true;
}


void distribute_ack(void)
{
  dpp_message_t* msg          = (dpp_message_t*)msg_buffer;
  uint32_t       msg_len      = 0;
  bool           is_initiator = false;

  if (is_reporter) {
    /* get message from receive queue */
    if (xQueueReceive(xQueueHandle_lora_rx, msg_buffer, 0)) {
      msg_len = DPP_MSG_LEN(msg);
      /* check whether message is valid */
      if (msg->header.payload_len > 0 &&
          msg->header.type == DPP_MSG_TYPE_ACK_COMMAND &&
          msg_len <= DPP_MSG_PKT_LEN) {
        /* change the target ID to broadcast, adjust the generation time and recalculate the CRC */
        msg->header.target_id       = DPP_DEVICE_ID_BROADCAST;
        msg->header.generation_time = get_time(0) + DISTR_ACKCMD_TIMESTAMP_OFS_US;  // add a few us to account for processing delay below
        ps_update_msg_crc(msg);
        is_initiator = true;
      } else {
        msg_len      = 0;
        is_initiator = false;
        LOG_WARNING("ACK is invalid (len: %u  type: %u)", msg->header.payload_len, msg->header.type);
      }
    } else {
      msg_len = 0;
      LOG_WARNING("no ACK to distribute");
    }
  }
  /* send / receive */
  gloria_start(is_initiator, msg_buffer, msg_len, GLORIA_INTERFACE_N_TX, 1);
  vTaskDelay(pdMS_TO_TICKS(DATA_AGGREGATION_SLOT_TIME_MS));
  gloria_stop();

  if (is_reporter) {
    if (msg_len) {
      process_message(msg, 0);
    }
  } else if (gloria_get_rx_cnt() && gloria_is_t_ref_updated() && (msg->header.type == DPP_MSG_TYPE_ACK_COMMAND)) {
    LOG_INFO("ACK received with timestamp %llu", msg->header.generation_time);
    if (process_message(msg, 0)) {
      /* use timestamp of received message to update the local time */
      set_time(msg->header.generation_time, gloria_get_t_ref_hs(), !DISTR_ACKCMD_CONT_SLOT);
    }
  } else {
    LOG_WARNING("no ACK received");
  }

#if DISTR_ACKCMD_CONT_SLOT
  vTaskDelay(pdMS_TO_TICKS(DATA_AGGREGATION_GAP_TIME_MS));
  is_initiator  = false;
  msg_buffer[0] = 0;
  msg_len       = 1;
  /* check the elapsed time to the last captured timestamp request event */
  if (trg_pending) {
    /* new time request occurred in the meantime */
    LOG_VERBOSE("participating in contention...");
    is_initiator = true;
    trg_pending  = false;
  }
  /* contention slot */
  gloria_start(is_initiator, msg_buffer, msg_len, GLORIA_INTERFACE_N_TX, 1);
  vTaskDelay(pdMS_TO_TICKS(DATA_AGGREGATION_SLOT_TIME_MS));
  gloria_stop();
  if (!is_reporter) {
    /* let the timesync task run to handle pending time requests (NOTE: the reporter already executed the timesync task before, don't call it here again) */
    xTaskNotifyGive(xTaskHandle_timesync);
  }
  /* if data has been sent or data detected, go back to waiting for data aggregation */
  if (is_initiator || gloria_get_rx_started_cnt()) {
    schedule_timeout(0, DATA_AGGREGATION_DELAY_MS - DATA_AGGREGATION_GUARD_TIME_MS);
    com_state = COM_STATE_WAIT_FOR_DATA_AGGREGATION;
    NOTIFY_COM_TASK();
    LOG_INFO("contention detected");
  } else {
    END_EVENT();
  }
#else /* DISTR_ACKCMD_CONT_SLOT */
  END_EVENT();
#endif /* DISTR_ACKCMD_CONT_SLOT */
}


void aggregate_data(bool health_data)
{
  /* make sure the TX queue is empty */
  xQueueReset(xQueueHandle_fsk_tx);
#if BOLT_ENABLE
  /* let the BOLT task run */
  xTaskNotifyGive(xTaskHandle_bolt);
  vTaskDelay(pdMS_TO_TICKS(BOLT_TASK_YIELD_TIME_MS));    // yield
#else /* BOLT_ENABLE */
  if (!health_data) {
    generate_geo_acq_msg();     // generate a dummy acquisition message
  }
#endif /* BOLT_ENABLE */
  if (health_data) {
    /* before distributing the data, do local node health aggregation */
    generate_node_health_min(INTERFACE_FSK);
  }
  /* start data collection / aggregation */
  if (!collect_data()) {
    return;
  }
  /* make sure the TX queue is empty before inserting the aggregated message */
  xQueueReset(xQueueHandle_lora_tx);
  if (health_data) {
    generate_aggr_message(DPP_MSG_TYPE_HEALTH_MIN);
  } else {
    generate_aggr_message(DPP_MSG_TYPE_GEOPHONE_ACQ);
  }
  /* decision making: is the event interesting? */
  if (xQueuePeek(xQueueHandle_lora_tx, msg_buffer, 0) &&
      analyze_aggr_message((dpp_message_t*)msg_buffer)) {
    if (is_reporter) {
      com_state = COM_STATE_SEND_TO_BS;
      NOTIFY_COM_TASK();
    } else {
      /* schedule communication round for ACK / command / timestamp distribution */
      schedule_timeout(ref_time, DISTR_ACKCMD_DELAY_MS);
      com_state = COM_STATE_WAIT_FOR_COMMAND;
      NOTIFY_COM_TASK();      // not really necessary since there is no work to do in this state
    }
  } else {
    xQueueReset(xQueueHandle_lora_tx);    /* make sure the TX queue is empty */
    LOG_INFO("no data to send to the base station");
    END_EVENT();
  }
}


/* ================ Radio Callback Functions ================ */

void on_radio_tx_done(void)
{
  //LOG_VERBOSE("TX done");

  if (com_state == COM_STATE_SEND_WAKEUP_MESSAGE) {
    /* inform the com task that the com_state has changed */
    com_state = COM_STATE_WAIT_FOR_LEADER_ELECTION;
    NOTIFY_COM_TASK_FROM_ISR();     // not really necessary since there is no work to do in this state

  } else if (com_state == COM_STATE_SEND_TO_BS) {
    /* after sending the message, go to RX and listen for an ACK/command from the base station */
    com_state = COM_STATE_WAIT_FOR_BS_ACK;
    NOTIFY_COM_TASK_FROM_ISR();
  }
}


void on_radio_rx_done(uint8_t* payload, uint16_t size, int16_t rssi, int8_t snr, bool crc_error)
{
  //LOG_VERBOSE("RX done");

  if (com_state == COM_STATE_STAGGERED_WAKEUP || com_state == COM_STATE_WAKEUP) {
    dpp_message_min_t* wakeup_msg = (dpp_message_min_t*)payload;
    if (wakeup_msg->header.type == DPP_MSG_TYPE_STAG_WAKEUP) {
      LOG_INFO("Wakeup message from Node ID: %i", wakeup_msg->header.device_id);
      com_state = COM_STATE_WAIT_FOR_LEADER_ELECTION;
      /* transition to leader election state will be handled by the timeout function */
      schedule_timeout(0, LEADER_ELECTION_OFFSET_MS);
      NOTIFY_COM_TASK_FROM_ISR();
    }

  } else if (com_state == COM_STATE_WAIT_FOR_BS_ACK) {
    /* receive message from the base station */
    dpp_message_t* rcvd_msg = (dpp_message_t*)payload;
    cancel_timeout();
    /* does the target ID match? */
    if (crc_error || rcvd_msg->header.type != DPP_MSG_TYPE_ACK_COMMAND || rcvd_msg->header.target_id != config.node_id) {
      /* resend message */
      LOG_WARNING("invalid ACK received");
      com_state = COM_STATE_SEND_TO_BS;
      NOTIFY_COM_TASK_FROM_ISR();
    } else {
      /* successfully received ACK from base station */
      xQueueReceiveFromISR(xQueueHandle_lora_tx, msg_buffer, NULL);    // remove element from the queue
      if (send_bs_fail_cnt <= SEND_BS_FALLBACK_THRESHOLD) {
        send_bs_fail_cnt = 0;     // only reset to zero if threshold has not yet been crossed
      } else if (send_bs_fail_cnt <= SEND_BS_FALLBACK_2_THRESHOLD) {
        send_bs_fail_cnt = SEND_BS_FALLBACK_THRESHOLD + 1;
      }
      LOG_INFO("ACK received (timestamp: %llu)", rcvd_msg->header.generation_time);
      xQueueSendFromISR(xQueueHandle_lora_rx, payload, 0);
      /* store the received UNIX timestamp together with the radio interrupt timestamp */
      set_time(rcvd_msg->header.generation_time, radio_get_last_sync_timestamp() - gloria_timings[send_bs_modulation].txSync, true);

      schedule_timeout(ref_time, DISTR_ACKCMD_DELAY_MS);
      com_state = COM_STATE_WAIT_FOR_COMMAND;
      NOTIFY_COM_TASK_FROM_ISR();
    }
  } else {
    LOG_WARNING("unexpected RX done interrupt");
  }
}


void on_radio_cad_done(bool detected)
{
  LOG_VERBOSE("CAD done");

  if (detected) {
    /* switch to receive mode */
    radio_receive(0);

  } else {

    switch (com_state) {

    case COM_STATE_STAGGERED_WAKEUP:
      if (STAG_WAKEUP_MODE == WAKEUP_MODE_LORA_DC_CAD) {
        lpm_update_opmode(OP_MODE_EVT_DONE);
      }
      break;

    case COM_STATE_WAKEUP:
      /* no CAD detected -> time to send the wakeup message */
      com_state = COM_STATE_SEND_WAKEUP_MESSAGE;
      NOTIFY_COM_TASK_FROM_ISR();
      break;

    default:
      break;
    }
  }
}


void on_radio_timeout(bool crc_error)
{
  LOG_VERBOSE("radio timeout");

  if (com_state == COM_STATE_WAIT_FOR_BS_ACK) {
    /* Timeout reached because no ACK from the base station received
     * Go back to SEND_TO_BS and try to resend the message */
    com_state = COM_STATE_SEND_TO_BS;
    NOTIFY_COM_TASK_FROM_ISR();
  }
}


void wakeup_flood_finished(void)
{
  //LOG_VERBOSE("Gloria flood finished");

  /* gloria_stop() has already been called at this point */
  if (gloria_is_t_ref_updated() && (((dpp_message_t*)msg_buffer)->header.type == DPP_MSG_TYPE_STAG_WAKEUP)) {
    LOG_VERBOSE("wakeup message received");
    schedule_timeout(gloria_get_t_ref(), LEADER_ELECTION_OFFSET_MS);
    com_state = COM_STATE_WAIT_FOR_LEADER_ELECTION;
    NOTIFY_COM_TASK_FROM_ISR();
  } else {
    LOG_WARNING("received invalid wakeup packet");
    /* if there is still enough time, restart Gloria RX */
    if (ms_until_timeout() >= STAG_WAKEUP_SLOT_MS) {
      gloria_register_flood_callback(wakeup_flood_finished);
      gloria_start(false, msg_buffer, 0, GLORIA_INTERFACE_N_TX, 1);
    }
    /* do not cancel timeout */
  }
}


void discosync_finished(void)
{
  if (discosync_get_rx_cnt() > 0 || codetection_th < 2) {          // TODO could be optimized to skip leader election and data aggregation
#if LEADER_ELECTION_ALIGN_SLOTS
    // set the ref. time to the start of the leader election (in hs ticks)
    ref_time = discosync_get_t_ref() + HS_TIMER_FREQUENCY_MS * (LEADER_ELECTION_OFFSET_MS + (NUM_HOPS + 1) * DISCOSYNC_SLOT_TIME_MS * DISCOSYNC_NUM_SLOTS + 1);   // + 1 for slack
#endif /* LEADER_ELECTION_ALIGN_SLOTS */
    uint64_t lp_now, hs_now;
    lptimer_now_synced(&lp_now, &hs_now);
    schedule_timeout(lp_now - (hs_now - discosync_get_t_ref()) * LPTIMER_SECOND / HS_TIMER_FREQUENCY, LEADER_ELECTION_OFFSET_MS + (NUM_HOPS + 1) * DISCOSYNC_SLOT_TIME_MS * DISCOSYNC_NUM_SLOTS);
    com_state = COM_STATE_WAIT_FOR_LEADER_ELECTION;
    NOTIFY_COM_TASK_FROM_ISR();
  } else {
    LOG_WARNING("no neighbors found");
    END_EVENT_FROM_ISR();
  }
}



/*================ Leader Election Function ================*/

void leader_election(void)
{
  uint32_t slot_counter  = 0;
  uint32_t round_counter = (uint8_t)ceil((log2(MAX_NUM_NODES) / log2(LEADER_ELECTION_MODE_BASE)));
  uint16_t leader_id     = 0;
  bool     is_sender     = false;

#define LEADER_ELECTION_PAYLOAD_LEN   1

  is_leader = true;

  if (LEADER_ELECTION_MODE == LEADER_ELECTION_MODE_BINARY_SEARCH) {
    slot_counter = log2(MAX_NUM_NODES);
  } else if (LEADER_ELECTION_MODE >= LEADER_ELECTION_MODE_BASE_2) {
    slot_counter = LEADER_ELECTION_MODE_BASE;
  } else {
    slot_counter = MAX_NUM_NODES;
  }

  while (true) {

    if (check_if_sending_slot(slot_counter, round_counter) && is_leader) {
      /* start gloria flood as transmitter */
      is_sender = true;
      leader_id = (leader_id << 1) | 1;
      msg_buffer[0] = slot_counter;

    } else {
      /* start gloria in receive mode */
      is_sender   = false;
      leader_id <<= 1;
    }

#if LEADER_ELECTION_ALIGN_SLOTS
    /* synchronize to the ref. time */
    if (ref_time) {
      uint32_t curr_ticks = hs_timer_get_counter();
      while ((uint32_t)((uint32_t)ref_time - curr_ticks) < HS_TIMER_FREQUENCY_MS * LEADER_ELECTION_GAP_TIME_MS) {
        curr_ticks = hs_timer_get_counter();
      }
      ref_time += HS_TIMER_FREQUENCY_MS * (LEADER_ELECTION_GAP_TIME_MS + LEADER_ELECTION_SLOT_TIME_MS);
    }
#endif /* LEADER_ELECTION_ALIGN_SLOTS */

    gloria_start(is_sender, msg_buffer, LEADER_ELECTION_PAYLOAD_LEN, GLORIA_INTERFACE_N_TX, 1);
    /* wait until the end of the communication slot */
    vTaskDelay(pdMS_TO_TICKS(LEADER_ELECTION_SLOT_TIME_MS));  // or use: delay_com_task(t_slot);
    gloria_stop();                                            // reached end of slot -> stop gloria

    if (!is_sender) {
      /* node was in receive mode */
      if (gloria_get_rx_cnt() && (gloria_get_payload_len() == LEADER_ELECTION_PAYLOAD_LEN)) {
        uint8_t rcvd_slot_counter = msg_buffer[0];  // first byte of the received packet is supposed to be the slot counter
        if (rcvd_slot_counter != slot_counter) {
          LOG_WARNING("invalid slot counter received in leader election (expected %u and received %u)", slot_counter, rcvd_slot_counter);
        } else {
          leader_id |= 1;
          is_leader  = false;

          if (LEADER_ELECTION_MODE == LEADER_ELECTION_MODE_LINEAR) {
            is_sender = true;
          }
          if (LEADER_ELECTION_MODE >= LEADER_ELECTION_MODE_BASE_2) {
            slot_counter = 1;
          }
        }
      }
    } else {
      /* node was sender, reset variable */
      if (LEADER_ELECTION_MODE >= LEADER_ELECTION_MODE_BASE_2) {
        slot_counter = 1;
      }
      is_sender = false;
    }

    /* check whether leader election is finished */
    if ( ((LEADER_ELECTION_MODE == LEADER_ELECTION_MODE_LINEAR)        && is_sender) ||
         ((LEADER_ELECTION_MODE == LEADER_ELECTION_MODE_BINARY_SEARCH) && (slot_counter == 0)) ||
         ((LEADER_ELECTION_MODE >= LEADER_ELECTION_MODE_BASE_2)        && (slot_counter == 0 && round_counter == 0)) ) {
      break;    // leader election finished
    }

    /* update round counter */
    if (slot_counter == 1) {
      if (round_counter > 0) {
        round_counter--;
      }
    }
    /* update slot counter */
    if (LEADER_ELECTION_MODE >= LEADER_ELECTION_MODE_BASE_2) {
      if (slot_counter == 1) {
        if (round_counter > 0) {
          slot_counter = LEADER_ELECTION_MODE_BASE;
        } else {
          break;    // leader election finished
        }
      } else {
        slot_counter--;
      }
    } else if (slot_counter == 1) {
      break;        // leader election finished
    } else {
      slot_counter--;
    }

    /* add gap time (required, otherwise the slots will overlap and data will be misinterpreted) */
#if !LEADER_ELECTION_ALIGN_SLOTS
    vTaskDelay(pdMS_TO_TICKS(LEADER_ELECTION_GAP_TIME_MS));
#endif /* LEADER_ELECTION_ALIGN_SLOTS */
  }

  LOG_VERBOSE("leader is %u", leader_id + MIN_NODE_ID);
}


/*================ Helper Functions ================*/


void notify_com_task(com_task_notify_t val, bool overwrite, bool from_isr)
{
  if (from_isr) {
    xTaskNotifyFromISR(xTaskHandle_com, val, overwrite ? eSetValueWithOverwrite : eSetValueWithoutOverwrite, NULL);
  } else {
    xTaskNotify(xTaskHandle_com, val, overwrite ? eSetValueWithOverwrite : eSetValueWithoutOverwrite);
  }
}


void resume_com_task_from_isr(void)
{
  NOTIFY_COM_TASK_FROM_ISR();
}


/* NOTE: this function runs in an ISR context */
void timeout_callback(void)
{
  bool notify = false;

  /* make sure the MCU is awake */
  lpm_update_opmode(OP_MODE_EVT_WAKEUP);

  switch (com_state) {

  case COM_STATE_STAGGERED_WAKEUP:
    if (STAG_WAKEUP_MODE == WAKEUP_MODE_FSK) {
      gloria_stop();
      if (gloria_is_t_ref_updated() && (((dpp_message_t*)msg_buffer)->header.type == DPP_MSG_TYPE_STAG_WAKEUP)) {
        LOG_VERBOSE("wakeup message received");
        schedule_timeout(gloria_get_t_ref(), LEADER_ELECTION_OFFSET_MS);
        com_state = COM_STATE_WAIT_FOR_LEADER_ELECTION;
      } else {
        com_state = COM_STATE_SEND_WAKEUP_MESSAGE;
      }
    } else {
      /* move to next phase */
      com_state = COM_STATE_WAKEUP;
    }
    notify = true;
    break;

  case COM_STATE_WAIT_FOR_LEADER_ELECTION:
    /* move to next phase */
    com_state = COM_STATE_LEADER_ELECTION;
    notify = true;
    break;

  case COM_STATE_WAIT_FOR_DATA_AGGREGATION:
    com_state = COM_STATE_DATA_AGGREGATION;
    notify = true;
    break;

  case COM_STATE_SEND_TO_BS:
    /* don't change state */
    notify = true;
    break;

  case COM_STATE_WAIT_FOR_BS_ACK:
    LOG_VERBOSE("RX timeout");
    com_state = COM_STATE_SEND_TO_BS;
    notify = true;
    break;

  case COM_STATE_WAIT_FOR_COMMAND:
    com_state = COM_STATE_PROCESS_COMMAND;
    notify = true;
    break;

  default:
    /* timeout -> abort and go back to idle state */
    LOG_WARNING("timeout in state %u (aborting)", com_state);
    END_EVENT_FROM_ISR();
    break;
  }

  if (notify) {
    NOTIFY_COM_TASK_FROM_ISR();
  }
}


void schedule_timeout(uint64_t timestamp, uint32_t offset_ms)
{
  if (!timestamp) {
    /* use a relative offset */
    timestamp = lptimer_now();
  }
  lptimer_set(timestamp + LPTIMER_MS_TO_TICKS(offset_ms), timeout_callback);
}


/* returns the remaining time before the scheduled timeout occurs, in ms */
uint32_t ms_until_timeout(void)
{
  uint64_t scheduled = lptimer_get();
  uint64_t now       = lptimer_now();
  if (now >= scheduled) {
    return 0;
  }
  return LPTIMER_TICKS_TO_MS(scheduled - now);
}


void cancel_timeout(void)
{
  lptimer_set(0, 0);
}


void delay_com_task(uint32_t ms)
{
  /* schedule task notification in x ms and suspend task */
  lptimer_set(lptimer_now() + LPTIMER_MS_TO_TICKS(ms), resume_com_task_from_isr);
  ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
}


bool is_channel_free(void)
{
  if (!send_bs_cad_mode) {
    return true;
  }

#if SEND_BS_CHANNEL_FREE_RSSI_TH != 0
  /* NOTE: RX config needs to be set before calling this function */

  /* use RSSI-based detection */
  uint16_t counter      = 10;
  uint16_t occupied_cnt = 0;

  radio_set_rx_callback(0);
  radio_set_timeout_callback(0);

  while (counter) {
    /* only works reliably if radio is put into receive mode each time */
    radio_standby();
    radio_receive(false, false, 0, 0);
    /* wait until receive mode is initialized */
    vTaskDelay(pdMS_TO_TICKS(3));

    int32_t rssi = radio_get_rssi();
    if (rssi > SEND_BS_CHANNEL_FREE_RSSI_TH) {
      occupied_cnt++;
    }
    counter--;
  }
  radio_standby();

  /* only return 0 (channel is occupied) when more than half of the measurement is above the threshold */
  return (occupied_cnt < (10 / 2));

#else  /* SEND_BS_CHANNEL_FREE_RSSI_TH */

  return radio_is_channel_free(send_bs_modulation, 200);   // 200ms timeout -> should normally return before timeout occurs

#endif /* SEND_BS_CHANNEL_FREE_RSSI_TH */
}


void set_rx_tx_config(void)
{
  /* set callback functions */
  radio_set_rx_callback(&on_radio_rx_done);
  radio_set_tx_callback(&on_radio_tx_done);
  radio_set_cad_callback(&on_radio_cad_done);
  radio_set_timeout_callback(&on_radio_timeout);
  radio_set_irq_mode(IRQ_MODE_ALL);
  radio_set_rx_gain(true);

  if (com_state == COM_STATE_STAGGERED_WAKEUP || com_state == COM_STATE_WAKEUP) {
    if (STAG_WAKEUP_MODE != WAKEUP_MODE_FSK) {
      radio_set_config_rxtx(true,
                            STAG_WAKEUP_LORA_BAND,
                            radio_modulations[STAG_WAKEUP_LORA_MODULATION].datarate,
                            STAG_WAKEUP_LORA_TX_POWER,
                            radio_modulations[STAG_WAKEUP_LORA_MODULATION].bandwidth,
                            radio_modulations[STAG_WAKEUP_LORA_MODULATION].preambleLen,
                            radio_modulations[STAG_WAKEUP_LORA_MODULATION].coderate,
                            0,
                            false,
                            0,
                            true);
    }
    /* else: nothing to configure (done by gloria interface) */

  } else if (com_state == COM_STATE_SEND_TO_BS || com_state == COM_STATE_WAIT_FOR_BS_ACK) {
    uint8_t rfband = SEND_BS_LORA_BAND;
    int8_t  txpwr  = send_bs_tx_pwr;
    if (send_bs_fail_cnt > SEND_BS_FALLBACK_THRESHOLD) {
      LOG_VERBOSE("using fallback radio config");
      send_bs_modulation = SEND_BS_LORA_FALLBACK_MODULATION;
      rfband             = SEND_BS_LORA_FALLBACK_BAND;
      if (send_bs_fail_cnt > SEND_BS_FALLBACK_2_THRESHOLD) {    // if threshold exceeded in fallback mode, then set TX power to max
        txpwr            = RADIO_MAX_POWER;
        LOG_VERBOSE("TX power set to %ddBm", txpwr);
      }
    }
    radio_set_config_rxtx(true,
                          rfband,
                          radio_modulations[send_bs_modulation].datarate,
                          txpwr,
                          radio_modulations[send_bs_modulation].bandwidth,
                          radio_modulations[send_bs_modulation].preambleLen,
                          radio_modulations[send_bs_modulation].coderate,
                          0,
                          false,
                          0,
                          true);
  }
}


bool check_if_sending_slot(uint16_t slot_counter, uint8_t round_counter)
{
  uint32_t node_id_ofs = config.node_id - MIN_NODE_ID;
  if (LEADER_ELECTION_MODE == LEADER_ELECTION_MODE_LINEAR) {
    return (slot_counter == node_id_ofs);
  }
  if (LEADER_ELECTION_MODE == LEADER_ELECTION_MODE_BINARY_SEARCH) {
    return (((node_id_ofs >> (slot_counter - 1)) & 0x1));
  }
  if (LEADER_ELECTION_MODE >= LEADER_ELECTION_MODE_BASE_2) {
    //      node_id_ofs  >>                 Shifting Parameter                               &  Mask                            == if sending slot?
    return ((node_id_ofs >> ((round_counter - 1) * (uint8_t)log2(LEADER_ELECTION_MODE_BASE)) & (LEADER_ELECTION_MODE_BASE - 1)) == (slot_counter - 1));
  }
  if (LEADER_ELECTION_MODE == LEADER_ELECTION_MODE_BASE_10) {
    //                 node_id_ofs /  10^(round_counter - 1) modulo 10 to get the currently interested decimal number
    return (((uint8_t)(node_id_ofs / (pow(10, round_counter - 1))) % 10)) == (slot_counter - 1);
  }
  return false;
}


/* returns the difference between two node IDs, with wrap around
 * NOTE: id1 and id2 must be in the range [MIN_NODE_ID, MIN_NODE_ID + MAX_NUM_NODES - 1] */
uint16_t calc_nodeid_diff(uint16_t id1, uint16_t id2)
{
  if (id1 > id2) {
    return (uint16_t)MIN((int32_t)(id1 - id2), ((int32_t)id2 - id1 + MAX_NUM_NODES));
  } else {
    return (uint16_t)MIN((int32_t)(id2 - id1), ((int32_t)id1 - id2 + MAX_NUM_NODES));
  }
}


void collect_radio_stats(void)
{
  if (gloria_get_rx_cnt()) {
    uint8_t curr_hop_cnt = gloria_get_rx_index() + 1;
    if (curr_hop_cnt > radio_stats.hop_cnt_max) {
      radio_stats.hop_cnt_max = curr_hop_cnt;
    }
    radio_stats.rssi_sum    += gloria_get_rssi();
    radio_stats.snr_sum     += gloria_get_snr();
    radio_stats.num_samples++;
  }
}


void get_radio_stats(int8_t* out_avg_rssi, uint8_t* out_avg_snr, uint8_t* out_max_hops, bool reset_stats)
{
  if (radio_stats.num_samples) {
    if (out_avg_rssi) {
      *out_avg_rssi = (int8_t)(radio_stats.rssi_sum / radio_stats.num_samples);
    }
    if (out_avg_snr) {
      *out_avg_snr = (uint8_t)(radio_stats.snr_sum / radio_stats.num_samples);
    }
    if (out_max_hops) {
      *out_max_hops = (uint8_t)(radio_stats.hop_cnt_max);
    }
  }
  if (reset_stats) {
    memset(&radio_stats, 0, sizeof(radio_stats_t));
  }
}


uint64_t get_reference_timestamp(void)
{
  return ref_time;
}


#endif /* !BASESTATION */
