/*
 * Copyright (c) 2020 - 2021, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * reads messages from BOLT
 */

#include "main.h"


#if BOLT_ENABLE

/* Global variables ----------------------------------------------------------*/


/* Private define ------------------------------------------------------------*/

#ifndef BOLT_TASK_RESUMED
#define BOLT_TASK_RESUMED()
#define BOLT_TASK_SUSPENDED()
#endif /* BOLT_TASK_RESUMED */


/* Private variables and functions -------------------------------------------*/


/* Functions -----------------------------------------------------------------*/

void task_bolt(void const * argument)
{
  LOG_VERBOSE("bolt task started");

  /* empty the BOLT queue */
  bolt_flush();

#if !BASESTATION
  /* configure BOLT TREQ in EXTI mode */
  GPIO_InitTypeDef GPIO_InitStruct = { 0 };
  GPIO_InitStruct.Pin = COM_TREQ_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(COM_TREQ_GPIO_Port, &GPIO_InitStruct);
  HAL_NVIC_SetPriority(EXTI3_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(EXTI3_IRQn);
#endif /* BASESTATION */

  /* Infinite loop */
  for (;;)
  {
    /* wait for notification token (= explicitly granted permission to run) */
    BOLT_TASK_SUSPENDED();
    ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
    BOLT_TASK_RESUMED();

    /* read from BOLT */
    static uint8_t  bolt_read_buffer[BOLT_MAX_MSG_LEN];
    uint32_t max_read_cnt = BOLT_MAX_READ_COUNT;
    /* only read as long as there is still space in the transmit queue */
    while (max_read_cnt && BOLT_DATA_AVAILABLE) {
      uint32_t len = bolt_read(bolt_read_buffer);
      if (!len) {
        LOG_ERROR("bolt read failed");
        EVENT_ERROR(EVENT_SX1262_BOLT_ERROR, 0);
        break;
      }
      process_message((dpp_message_t*)bolt_read_buffer, true);
      max_read_cnt--;
    }
    if (max_read_cnt < BOLT_MAX_READ_COUNT) {
      LOG_INFO("%lu msg read from BOLT", BOLT_MAX_READ_COUNT - max_read_cnt);
    }

#if BASEBOARD_TREQ_WATCHDOG && BASEBOARD && BASESTATION
    static uint64_t last_treq = 0;
    if (get_captured_timestamp() > last_treq) {
      last_treq = get_captured_timestamp();
    }
    /* only use time request watchdog when baseboard is enabled */
    if (BASEBOARD_IS_ENABLED()) {
      bool powercycle = false;
      /* check when was the last time we got a time request */
      if (((hs_timer_now() - last_treq) / HS_TIMER_FREQUENCY) > BASEBOARD_TREQ_WATCHDOG) {
        last_treq = hs_timer_now();
        powercycle = true;
      }
      if (powercycle) {
        /* power cycle the baseboard */
        LOG_WARNING("power-cycling baseboard (TREQ watchdog)");
        BASEBOARD_DISABLE();
        /* enable pin must be kept low for ~1s -> schedule pin release */
        if (!schedule_bb_command((get_time(0) / 1000000) + 2, CMD_SX1262_BASEBOARD_ENABLE, 0)) {
          /* we must wait and release the reset here */
          LOG_WARNING("failed to schedule baseboard enable");
          delay_us(1000000);
          BASEBOARD_ENABLE();
        }
      }
    } else {
      last_treq = hs_timer_now();
    }
#endif /* BASEBOARD_TREQ_WATCHDOG */

    //LOG_VERBOSE("bolt task executed");
  }
}

#endif /* BOLT_ENABLE */
