/*
 * Copyright (c) 2020 - 2021, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * debug task (serial printing, stats, periodic checks, RTC wakeup scheduling, ...)
 */

#include "main.h"


/* Global variables ----------------------------------------------------------*/

extern TaskHandle_t      xTaskHandle_debug;
extern SemaphoreHandle_t xSemHandle_rtc;
extern bool              abort_event_on_rtc_trigger;
extern uint16_t          health_msg_period;
extern uint8_t           send_bs_modulation;
extern int8_t            send_bs_tx_pwr;
#if FW_OTA_ENABLE
extern bool              fw_update_mode;
#endif /* FW_OTA_ENABLE */


/* Private define ------------------------------------------------------------*/

#ifndef DEBUG_TASK_RESUMED
#define DEBUG_TASK_RESUMED()
#define DEBUG_TASK_SUSPENDED()
#endif


/* Private variables ---------------------------------------------------------*/


/* Functions -----------------------------------------------------------------*/

#if !BASESTATION

void rtc_wakeup_cb(void)
{
  LOG_VERBOSE("RTC wakeup");

  /* send notification to the communication task */
  notify_com_task(COM_TASK_NOTIFY_RTC, abort_event_on_rtc_trigger, true);
}

void schedule_node_health(void)
{
  /* re-scheduling required? */
  uint32_t next_alarm = rtc_time_to_next_alarm();     // time to next alarm in seconds
  if ((next_alarm != 0) && ((next_alarm / 60) <= health_msg_period)) {
    return;   // do not update
  }
  /* calculate the next wakeup time */
  uint32_t h = 0, m = 0, s = 0;
  rtc_get_time(&h, &m, 0);
  m += h * 60;
  uint32_t next = m + health_msg_period - (m % health_msg_period);
#if !AGGREGATE_HEALTH_MESSAGES
  /* make sure each node has a unique wakeup time -> use node ID as offset */
  s = (config.node_id - MIN_NODE_ID) * NODE_HEALTH_REPORTING_OFS_S;
  next += (s / 60);
  s = s % 60;
#endif /* AGGREGATE_HEALTH_MESSAGES */
  h = (next / 60) % 24;
  m = next % 60;
  if (!rtc_set_alarm_daytime(h, m, s, rtc_wakeup_cb)) {
    LOG_ERROR("failed to set alarm for node health");
    EVENT_ERROR(EVENT_SX1262_RTC_ERROR, 2);
  }
}

#endif /* BASESTATION */


void task_debug(void const * argument)
{
  LOG_VERBOSE("debug task started");

#if !LOG_PRINT_IMMEDIATELY
  xTaskNotifyGive(xTaskHandle_debug);
#endif /* LOG_PRINT_IMMEDIATELY */

  /* Infinite loop */
  for(;;)
  {
    DEBUG_TASK_SUSPENDED();
    ulTaskNotifyTake(pdTRUE, portMAX_DELAY);   /* wait for notification token */
    DEBUG_TASK_RESUMED();

  #if BASEBOARD
    /* process pending commands */
    process_scheduled_commands();
  #endif /* BASEBOARD */

    /* check for critical stack usage or overflow */
    rtos_check_stack_usage();

    /* print some stats */
#if !BASESTATION
  #if LOG_ENABLE
    uint32_t cpu_dc      = rtos_get_cpu_dc();
    uint32_t radio_rx_dc = radio_get_rx_dc() / 100;
    uint32_t radio_tx_dc = radio_get_tx_dc() / 100;
  #endif /* LOG_ENABLE */
    LOG_INFO("CPU duty cycle: %u.%02u%%    radio duty cycle (RX/TX): %u.%02u%%/%u.%02u%%", (uint16_t)(cpu_dc / 100), (uint16_t)(cpu_dc % 100), (uint16_t)(radio_rx_dc / 100), (uint16_t)(radio_rx_dc % 100), (uint16_t)(radio_tx_dc / 100), (uint16_t)(radio_tx_dc % 100));
#endif /* BASESTATION */

    /* acquire RTC access */
    if (xSemaphoreTake(xSemHandle_rtc, pdMS_TO_TICKS(1100))) {    /* wait up to 1s */
      /* print the current time */
      uint32_t h = 0, m = 0, s = 0;
      rtc_get_time(&h, &m, &s);
      LOG_VERBOSE("current time is %lu (%02u:%02u:%02u)", (uint32_t)(get_time(0) / 1000000), h, m, s);

    #if (NODE_HEALTH_MSG_INTERVAL > 0) && !BASESTATION
      /* make sure an RTC wakeup for health data collection is scheduled */
      schedule_node_health();
    #endif /* SEND_NODE_HEALTH_MSG */

      xSemaphoreGive(xSemHandle_rtc);

    } else {
      LOG_ERROR("failed to get RTC semaphore");
      EVENT_ERROR(EVENT_SX1262_MUTEX_ERROR, 1);
    }

    /* check if messages have been dropped due to BOLT queue full (handle this here in the lowest priority task instead of the BOLT task) */
  #if BOLT_ENABLE
    uint32_t dropped_cnt = bolt_get_write_failed_cnt(false);
    if (dropped_cnt && !bolt_full()) {
      EVENT_WARNING(EVENT_SX1262_QUEUE_FULL, (dropped_cnt << 16) | 1);
      LOG_WARNING("BOLT queue was full (%u messages dropped)", dropped_cnt);
      bolt_get_write_failed_cnt(true);   /* reset counter */
    }
  #endif /* BOLT_ENABLE */

  #if FW_OTA_ENABLE && !BASESTATION
    if (fw_update_mode) {
      /* disable all unnecessary interrupts, RTC alarm and lptimer */
      HAL_NVIC_DisableIRQ(EXTI3_IRQn);
      rtc_set_alarm_daytime(0, 0, 0, 0);
      lptimer_set(0, 0);
      /* make sure the com task state machine is in idle */
      notify_com_task(COM_TASK_NOTIFY_ABORT, true, false);
      /* run FW updater */
      fw_ota(send_bs_modulation, (send_bs_modulation == SEND_BS_LORA_FALLBACK_MODULATION) ? SEND_BS_LORA_FALLBACK_BAND : SEND_BS_LORA_BAND, send_bs_tx_pwr);
      /* re-enable interrupts */
      HAL_NVIC_EnableIRQ(EXTI3_IRQn);
      fw_update_mode = false;
      xTaskNotifyGive(xTaskHandle_debug);
    }
  #endif /* FW_OTA_ENABLE */

    /* flush the log print queue */
#if !LOG_PRINT_IMMEDIATELY
    log_flush();
#endif /* LOG_PRINT_IMMEDIATELY */
  }
}
